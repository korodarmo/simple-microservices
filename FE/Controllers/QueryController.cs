﻿using Core.Flash2;
using ePM_frontend.Helpers;
using ePM_frontend.Models;
using ePM_frontend.Services.Master.Interfaces;
using ePM_frontend.Services.Master.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static ePM_frontend.Helpers.BaseHeader;

namespace ePM_frontend.Controllers
{
    [Route("Query")]
    public class QueryController : Controller
    {
        IMaster iMaster;

        public QueryController(IMaster _IMaster)
        {
            iMaster = _IMaster;
        }

        public IActionResult Index()
        {
            return Redirect("/Home");
        }
        
        #region 'CrudBasic'
        [Route("CrudBasic")]
        public IActionResult CrudBasic()
        {
            if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P1", "View"))
            {
                return Redirect("/Error/unAuthorize");
            }
            else
            {
                ViewBag.aksesAdd = GlobalFunction.CheckHakAkses(HttpContext, "N11P1", "Add");
                return View();
            }
        }
        [HttpPost("CrudBasic/getDataTables")]
        public async Task<IActionResult> getDataTablesCrudBasic(DataTableBindingModel Args)
        {
            try
            {
                var data = await iMaster.getDataCrudBasic(Args);

                if (data != null && string.IsNullOrEmpty(data?.error))
                {
                    return Ok(data);
                }
                else
                {
                    return StatusCode(500, new DataTableResultModel()
                    {
                        recordsTotal = 0,
                        data = new List<SkillModel>(),
                        draw = Args.draw,
                        error = data.error
                    });
                }
            }
            catch (Exception exception)
            {
                return StatusCode(500, new DataTableResultModel()
                {
                    recordsTotal = 0,
                    data = new List<SkillModel>(),
                    draw = Args.draw,
                    error = exception.Message
                });
            }
        }


        [HttpPost("CrudBasic/prosessData")]
        public async Task<IActionResult> prosessDataCrudBasic([FromBody] SkillModel poRecord)
        {
            Dictionary<string, string> vResult = new Dictionary<string, string>();
            try
            {
                if (poRecord.action.Equals("create"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P1", "Add"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.createCrudBasic(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("edit"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P1", "Update"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.updateCrudBasic(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("delete"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P1", "Delete"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.deleteCrudBasic(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else
                {
                    vResult.Add("errorcode", "500");
                    vResult.Add("msg", GlobalVariable.prosesFail);
                }

            }
            catch (Exception exc)
            {
                vResult.Add("errorcode", "500");
                vResult.Add("title", "Error");
                vResult.Add("msg", exc.Message);
            }

            return Ok(vResult);
        }


        [HttpGet("CrudBasic/list")]
        public async Task<IActionResult> getListCrudBasic(string term)
        {
            try
            {
                var data = await iMaster.getListCrudBasic(term);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(Result.Fail(ex.Message));
            }
        }
        #endregion

        #region 'Onetoone'
        [Route("Onetoone")]
        public IActionResult Onetoone()
        {
            if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P2", "View"))
            {
                return Redirect("/Error/unAuthorize");
            }
            else
            {
                ViewBag.aksesAdd = GlobalFunction.CheckHakAkses(HttpContext, "N11P2", "Add");
                return View();
            }
        }
        [HttpPost("Onetoone/getDataTables")]
        public async Task<IActionResult> getDataTablesOnetoone(DataTableBindingModel Args)
        {
            try
            {
                var data = await iMaster.getDataOnetoone(Args);

                if (data != null && string.IsNullOrEmpty(data?.error))
                {
                    return Ok(data);
                }
                else
                {
                    return StatusCode(500, new DataTableResultModel()
                    {
                        recordsTotal = 0,
                        data = new List<EmployeeModel>(),
                        draw = Args.draw,
                        error = data.error
                    });
                }
            }
            catch (Exception exception)
            {
                return StatusCode(500, new DataTableResultModel()
                {
                    recordsTotal = 0,
                    data = new List<EmployeeModel>(),
                    draw = Args.draw,
                    error = exception.Message
                });
            }
        }

        [HttpPost("Onetoone/prosessData")]
        public async Task<IActionResult> prosessDataOnetoone([FromBody] EmployeeModel poRecord)
        {
            Dictionary<string, string> vResult = new Dictionary<string, string>();
            try
            {
                if (poRecord.action.Equals("create"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P2", "Add"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.createOnetoone(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("edit"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P2", "Update"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.updateOnetoone(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("delete"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P2", "Delete"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.deleteOnetoone(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else
                {
                    vResult.Add("errorcode", "500");
                    vResult.Add("msg", GlobalVariable.prosesFail);
                }

            }
            catch (Exception exc)
            {
                vResult.Add("errorcode", "500");
                vResult.Add("title", "Error");
                vResult.Add("msg", exc.Message);
            }

            return Ok(vResult);
        }

        [HttpGet("Onetoone/list")]
        public async Task<IActionResult> getListOnetoone(string term)
        {
            try
            {
                var data = await iMaster.getListOnetoone(term);

                return Ok(data);
            }
            catch (Exception ex)
            {
                return BadRequest(Result.Fail(ex.Message));
            }
        }
        #endregion

        #region 'Onetomany'
        [Route("Onetomany")]
        public IActionResult Onetomany()
        {
            if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P3", "View"))
            {
                return Redirect("/Error/unAuthorize");
            }
            else
            {
                ViewBag.aksesAdd = GlobalFunction.CheckHakAkses(HttpContext, "N11P3", "Add");
                return View();
            }
        }
        [HttpPost("Onetomany/getDataTables")]
        public async Task<IActionResult> getDataTablesOnetomany(DataTableBindingModel Args)
        {
            try
            {
                var data = await iMaster.getDataOnetomany(Args);

                if (data != null && string.IsNullOrEmpty(data?.error))
                {
                    return Ok(data);
                }
                else
                {
                    return StatusCode(500, new DataTableResultModel()
                    {
                        recordsTotal = 0,
                        data = new List<EmployeeModel>(),
                        draw = Args.draw,
                        error = data.error
                    });
                }
            }
            catch (Exception exception)
            {
                return StatusCode(500, new DataTableResultModel()
                {
                    recordsTotal = 0,
                    data = new List<EmployeeModel>(),
                    draw = Args.draw,
                    error = exception.Message
                });
            }
        }


        [HttpPost("Onetomany/prosessData")]
        public async Task<IActionResult> prosessDataOnetomany([FromBody] EmployeeModel poRecord)
        {
            Dictionary<string, string> vResult = new Dictionary<string, string>();
            try
            {
                if (poRecord.action.Equals("create"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P3", "Add"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.createOnetomany(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("edit"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P3", "Update"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.updateOnetomany(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("delete"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P3", "Delete"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.deleteOnetomany(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else
                {
                    vResult.Add("errorcode", "500");
                    vResult.Add("msg", GlobalVariable.prosesFail);
                }

            }
            catch (Exception exc)
            {
                vResult.Add("errorcode", "500");
                vResult.Add("title", "Error");
                vResult.Add("msg", exc.Message);
            }

            return Ok(vResult);
        }
        #endregion

        #region 'Manytomany'
        [Route("Manytomany")]
        public IActionResult Manytomany()
        {
            if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P4", "View"))
            {
                return Redirect("/Error/unAuthorize");
            }
            else
            {
                ViewBag.aksesAdd = GlobalFunction.CheckHakAkses(HttpContext, "N11P4", "Add");
                return View();
            }
        }
        [HttpPost("Manytomany/getDataTables")]
        public async Task<IActionResult> getDataTablesManytomany(DataTableBindingModel Args)
        {
            try
            {
                var data = await iMaster.getDataManytomany(Args);

                if (data != null && string.IsNullOrEmpty(data?.error))
                {
                    return Ok(data);
                }
                else
                {
                    return StatusCode(500, new DataTableResultModel()
                    {
                        recordsTotal = 0,
                        data = new List<EmployeeModel>(),
                        draw = Args.draw,
                        error = data.error
                    });
                }
            }
            catch (Exception exception)
            {
                return StatusCode(500, new DataTableResultModel()
                {
                    recordsTotal = 0,
                    data = new List<EmployeeModel>(),
                    draw = Args.draw,
                    error = exception.Message
                });
            }
        }


        [HttpPost("Manytomany/prosessData")]
        public async Task<IActionResult> prosessDataManytomany([FromBody] EmployeeModel poRecord)
        {
            Dictionary<string, string> vResult = new Dictionary<string, string>();
            try
            {
                if (poRecord.action.Equals("create"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P4", "Add"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.createManytomany(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("edit"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P4", "Update"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.updateManytomany(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else if (poRecord.action.Equals("delete"))
                {
                    if (!GlobalFunction.CheckHakAkses(HttpContext, "N11P4", "Delete"))
                    {
                        vResult.Add("errorcode", "100");
                        vResult.Add("msg", GlobalVariable.notHaveAkses);
                    }
                    else
                    {
                        try
                        {
                            var response = await iMaster.deleteManytomany(BaseHeader.getBaseHeader(HttpContext), poRecord);
                            if (response.Succeeded == true && response.Data != null)
                            {
                                vResult.Add("errorcode", "0");
                                vResult.Add("msg", GlobalVariable.prosesSuccess);
                            }
                            else
                            {
                                vResult.Add("errorcode", "100");
                                vResult.Add("msg", response.Message);
                            }
                        }
                        catch (Exception exc)
                        {
                            vResult.Add("errorcode", "500");
                            vResult.Add("msg", exc.Message);
                        }
                    }
                }
                else
                {
                    vResult.Add("errorcode", "500");
                    vResult.Add("msg", GlobalVariable.prosesFail);
                }

            }
            catch (Exception exc)
            {
                vResult.Add("errorcode", "500");
                vResult.Add("title", "Error");
                vResult.Add("msg", exc.Message);
            }

            return Ok(vResult);
        }
        #endregion
    }
}
