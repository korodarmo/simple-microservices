﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ePM_frontend.Helpers;
using ePM_frontend.Helpers.Database;
using ePM_frontend.Middlewares;
using ePM_frontend.Models;
using ePM_frontend.Services.System.Interfaces;
using ePM_frontend.Services.System.Models;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ePM_frontend.Controllers
{
	[Route("_backyard")]
	public class BackyardController : Controller
	{
		readonly ISystem iSystem;

		public BackyardController(ISystem _iSystem)
		{
			iSystem = _iSystem;
		}

		[Route("login")]
		public IActionResult Login()
        {
			if (HttpContext.Session.GetString("Username") != null && HttpContext.Session.GetString("EmployeeNumber") != null)
			{
				return Redirect("/Home");
			}
            else
            {
				return View("Login");
			}
		}

		[Route("error")]
		public IActionResult Error()
		{
			string code = HttpContext.Session.GetString("ERRORCODE");
			if(code == "561")
            {
				return Redirect("/Error/unRegistered");
            }
			else if (code == "562")
			{
				return Redirect("/Error/unMapping");
			}
			else if (code == "563")
			{
				return Redirect("/Error/failedInsert");
			}
			else
            {
				return Redirect("/Error/failedException");
			}
		}

		[Route("validate")]
		[HttpPost]
		public async Task<IActionResult> validate([FromBody] LoginModel voData)
		{

			Console.WriteLine("validate");
			Dictionary<string, string> vResult = new Dictionary<string, string>();
			try
			{
				if (String.IsNullOrEmpty(voData.Username))
				{
					vResult.Add("errorcode", "100");
					vResult.Add("msg", "Username is Required");
				}
				else if (String.IsNullOrEmpty(voData.Password))
				{
					vResult.Add("errorcode", "100");
					vResult.Add("msg", "Password is Required");
				}
				else
				{
					string kdpUsername = voData.Username.ToLower();
					string kdpPassword = voData.Password.ToLower();
					try
					{
						var result = await iSystem.GetUserDataByUsername(kdpUsername);
						if (result.RecordID > 0)
						{
							string Password = GlobalFunction.CreateMD5Hash(kdpPassword);
							if (Password == result.Password)
                            {
								HttpContext.Session.SetString("Username", kdpUsername);
                                HttpContext.Session.SetString("UserID", kdpUsername);
                                HttpContext.Session.SetString("EmployeeNumber", kdpUsername);
								HttpContext.Session.SetString("Competency", "");
                                vResult.Add("errorcode", "0");
                            }
							else
                            {
								vResult.Add("errorcode", "100");
								vResult.Add("msg", "Password Incorrect");
                            }
						}
                        else
                        {
							vResult.Add("errorcode", "100");
							vResult.Add("msg", "User Not Found");
                        }

					}
					catch (Exception exc)
					{
						vResult.Add("errorcode", "500");
						vResult.Add("msg", exc.Message);
					}
				}
			}
			catch (Exception exc)
			{
				vResult.Add("errorcode", "500");
				vResult.Add("title", "Error");
				vResult.Add("msg", exc.Message);
			}

			return Ok(vResult);
		}

        [AllowAnonymous]
        [Route("logout")]
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();

            foreach (var cookie in Request.Cookies.Keys)
            {
                if (cookie == ".AspNetCore.Session")
                    Response.Cookies.Delete(cookie);
            }
			string allowLogin = Environment.GetEnvironmentVariable("ALLOW_LOGIN");
			if (allowLogin == null && string.IsNullOrEmpty(allowLogin))
			{
				return Redirect("/Error/logout");
			}
			else
			{
				return Redirect("/Home");
			}
		}
    }
}