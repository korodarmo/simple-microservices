﻿using Core.Flash2;
using ePM_frontend.Helper;
using ePM_frontend.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ePM_frontend.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult unAuthorize()
        {
            ViewData["Title"] = "UnAuthorized";
            ViewData["error_code"] = 401;
            ViewData["error_msg"] = "You Are Not Authorized";
            return View("Error");
        }

        public IActionResult unRegistered()
        {
            return Redirect(Environment.GetEnvironmentVariable("BASE_URL_SSO"));
            ViewData["Title"] = "UnAuthorized";
            ViewData["error_code"] = 561;
            ViewData["error_msg"] = "You Are Not Authorized";
            return View("Error");
        }

        public IActionResult unMapping()
        {
            //Error Not Registered / Mapping
            ViewData["Title"] = "UnAuthorized";
            ViewData["error_code"] = 562;
            ViewData["error_msg"] = "You Are Not Authorized";
            return View("Error");
        }

        public IActionResult failedInsert()
        {
            //Error Not Registered / Mapping
            ViewData["Title"] = "UnAuthorized";
            ViewData["error_code"] = 563;
            ViewData["error_msg"] = "You Are Not Authorized";
            return View("Error");
        }

        public IActionResult failedException()
        {
            //Error Not Registered / Mapping
            ViewData["Title"] = "UnAuthorized";
            ViewData["error_code"] = 565;
            ViewData["error_msg"] = "You Are Not Authorized";
            return View("Error");
        }
        public IActionResult logout()
        {
            ViewData["Title"] = "UnAuthorized";
            ViewData["error_code"] = 566;
            ViewData["error_msg"] = "You Are Log Out";
            return View("Error");
        }
    }
}
