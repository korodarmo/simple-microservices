﻿using ePM_frontend.Helpers;
using ePM_frontend.Models;
using ePM_frontend.Services.System.Interfaces;
using ePM_frontend.Services.System.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ePM_frontend.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {
        }
        public async Task<IActionResult> Index()
        {
            string view = "Index";
            return View(view);
        }
    
    }
}