﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using ePM_frontend.Helper;
using ePM_frontend.Services.System.Interfaces;
using ePM_frontend.Models;
using ePM_frontend.Helpers;
using ePM_frontend.Services.System.Models;
using System.Linq;
using System.IO;
using System.Text;

namespace ePM_frontend.Middlewares
{
    public class AuthMiddleware
    {
        private readonly RequestDelegate Next;
        private IConfiguration Configuration { get; }

        public AuthMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            Next = next;
            Configuration = configuration;
        }        

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                int Start, End;
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }

            return "";
        }
        public async Task InvokeAsync(HttpContext httpContext, ISystem _ISystem)
        {
            await stagingAppsAsync(httpContext, _ISystem);

            await Next(httpContext);
        }
        private async Task stagingAppsAsync(HttpContext httpContext, ISystem _ISystem)
        {
            if (httpContext.Session.GetString("Username") != null && httpContext.Session.GetString("EmployeeNumber") != null)
            {
                string Username = httpContext.Session.GetString("Username");
                if (httpContext.Session.GetString("RoleAccess") == null)
                {
                    var userData = await _ISystem.GetUserDataByUsername(Username);
                    httpContext.Session.SetString("UserID", userData.UserID);
                    httpContext.Session.SetString("Name", userData.Name);
                    httpContext.Session.SetString("RoleAccess", userData.RoleAccess);
                    httpContext.Session.SetString("RoleID", userData.Role);
                    httpContext.Session.SetString("Rolename", userData.Rolename);
                    httpContext.Session.SetString("Instance", userData.Instance);
                    httpContext.Session.SetInt32("OrganizationID", userData.OrganizationID);
                    httpContext.Session.SetString("OrganizationName", userData.OrganizationName);
                    httpContext.Response.Redirect("/Home");
                }
            }
            else
            {
                var path = httpContext.Request.Path;

                if (path.HasValue && !path.Value.StartsWith("/_backyard"))
                {
                    if (path.HasValue && !path.Value.StartsWith("/Error"))
                    {
                        httpContext.Response.Redirect("/_backyard/login");
                    }
                }
            }
        }
    }
}
