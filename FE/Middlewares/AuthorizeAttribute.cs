﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace ePM_frontend.Middlewares
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class Authorize : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {

            var Username = context.HttpContext.Session.GetString("Username");
            if (string.IsNullOrEmpty(Username))
            {
                context.Result = new RedirectToRouteResult(
                                   new RouteValueDictionary
                                   {
                                       { "action", "unAuthorize" },
                                       { "controller", "Error" }
                                   });
            }
        }
    }
}
