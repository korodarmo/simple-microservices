﻿using System.Collections.Generic;

namespace ePM_frontend.Models
{
    public class DataTableBindingModel
    {
        public DataTableBindingModel()
        {
            order = new List<DataTableOrderBindingModel>();
            search = new DataTableSearchBindingModel();
            columns = new List<DataTableColumnBindingModel>();
            advSearch = new Dictionary<string, string>();
        }

        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<DataTableOrderBindingModel> order { get; set; }
        public DataTableSearchBindingModel search { get; set; }
        public List<DataTableColumnBindingModel> columns { get; set; }
        public Dictionary<string, string> advSearch { get; set; }

        public int idParent { get; set; }
        public string jenis { get; set; }
    }
}