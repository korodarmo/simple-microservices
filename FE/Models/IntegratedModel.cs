﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ePM_frontend.Models
{
    public class EbridgingIOModel
    {
        public long RECID { get; set; }
        public string SITE { get; set; }
        public int ORGANIZATION_ID { get; set; }
        public string ORGANIZATION_NAME { get; set; }
    }
}
