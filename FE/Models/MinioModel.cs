using System;

namespace ePM_frontend.Models
{
    public class MinioModel
    {
        public bool succeeded { get; set; }
        public string message { get; set; }
        public byte[] data { get; set; }
        public string original_file_name { get; set; }
        public string new_file_name { get; set; }
        public string full_path_new_file { get; set; }
        public string content_type { get; set; }

    }
}
