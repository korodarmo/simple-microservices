﻿// using System.Text.Json.Serialization;

// namespace ePM_frontend.Models
// {
//     public enum IActivityLogStatus
//     {
//         ASSIGNED,
//         IN_PROGRESS,
//         PAUSED,
//         WAITING_APPROVAL,
//         FINISHED,
//         STOPPED,
//         CANCELED
//     }

//     public enum IDepartment
//     {
//         ENGINEERING,
//         PRODUCTION
//     }

//     public class ActivityLogModel
//     {
// #nullable enable
//         [JsonIgnore]
//         public long id { get; set; }
//         public long? boActivityId { get; set; } = null;
//         public string? shiftName { get; set; } = null;
//         public string? lineName { get; set; } = null;
//         public string? machineName { get; set; } = null;
//         public string? operationName { get; set; } = null;
//         public string? activityName { get; set; } = null;
//         public string? status { get; set; } = null;
//         public long? taskForceId { get; set; } = null;
//         public string? primaryAssignment { get; set; } = null;
//         public string? additionalProblem { get; set; } = null;
//         public string? additionalAction { get; set; } = null;
//         public int? outputGood { get; set; } = null;
//         public string? outputGoodUnit { get; set; } = null;
//         public int? outputDefect { get; set; } = null;
//         public string? outputDefectUnit { get; set; } = null;
//         public string? startedDate { get; set; } = null;
//         public string? startedBy { get; set; } = null;
//         public string? pausedDate { get; set; } = null;
//         public string? pausedBy { get; set; } = null;
//         public string? resumedDate { get; set; } = null;
//         public string? resumedBy { get; set; } = null;
//         public int? pauseTimeDiff { get; set; } = null;
//         public int? timeDiff { get; set; } = null;
//         [JsonIgnore]
//         public string? createdBy { get; set; }
//         [JsonIgnore]
//         public string? createdDate { get; set; }
//         [JsonIgnore]
//         public string? updatedBy { get; set; }
//         [JsonIgnore]
//         public string? updatedDate { get; set; }
//     }
// }
