﻿namespace ePM_frontend.Models
{
    public class DataTableColumnBindingModel
    {
        public DataTableColumnBindingModel()
        {
            search = new DataTableSearchBindingModel();
        }
        public string data { get; set; }
        public string name { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public DataTableSearchBindingModel search { get; set; }
    }
}