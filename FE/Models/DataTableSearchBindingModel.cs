﻿namespace ePM_frontend.Models
{
    public class DataTableSearchBindingModel
    {
        public string value { get; set; }
        public bool regex { get; set; }
    }
}