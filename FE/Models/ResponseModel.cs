using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ePM_frontend.Models
{
    public class ResponseModel
    {
        public object data { get; set; }
        public int TotalCount { get; set; }
        public long recordsTotal { get; set; }
        public long recordsFiltered { get; set; }
        public string Message { get; set; }
        public int status { get; set; }

        public ResponseModel(int status, string message, object data)
        {
            this.status = status;
            this.Message = message;
            this.data = data;

        }
    }
}