﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ePM_frontend.Models
{
    // public class NotifikasiModel
    // {
    //     [JsonIgnore]
    //     public long id { get; set; }
    //     [JsonIgnore]
    //     public DateTime CreatedAt { get; set; }
    //     public string AppKey { get; set; }
    //     public string Source { get; set; }
    //     public int Type { get; set; }
    //     public string Sender { get; set; }
    //     public string Recipient { get; set; }
    //     public string EmailRecipient { get; set; }
    //     public string Title { get; set; }
    //     public string Message { get; set; }
    //     public string RefID { get; set; }
    //     public string UIType { get; set; }
    //     public string RedirectURI { get; set; }
    //     public int isRead { get; set; }
    //     public int isDelete { get; set; }
    //     public string Additional { get; set; }
    //     public string status { get; set; }
    // }
    public class NotifikasiResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
