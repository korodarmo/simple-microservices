﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ePM_frontend.Models
{
    public class DatabaseActionResultModel
    {
        public bool Success { get; set; } 
        public string Message { get; set; }
        public object Payload { get; set; }
    }
}
