﻿namespace ePM_frontend.Models
{
    public class DataTableOrderBindingModel
    {
        public int column { get; set; }
        public string dir { get; set; }
    }
}