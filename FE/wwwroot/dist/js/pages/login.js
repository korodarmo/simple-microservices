$(document).ready(function() {
  $("#password i").on("click", function(event) {
    event.preventDefault();
    if ($("#password input").attr("type") == "text") {
      $("#password input").attr("type", "password");
      $("#password i").addClass("ion-ios-eye-off");
      $("#password i").removeClass( "ion-ios-eye");
    } else if ($("#password input").attr("type") == "password") {
      $("#password input").attr("type", "text");
      $("#password i").removeClass( "ion-ios-eye-off");
      $("#password i").addClass("ion-ios-eye");
    }
  });
});