var isLogin = false; //flag login

function processLogin() {
  var voData = {
    Username: $("#Username").val(),
    Password: $("#Password").val(),
  };
  if (voData.Username == "") {
    isAlert("Username wajib diisi terlebih dahulu");
  } else if (voData.Password == "") {
    isAlert("Password wajib diisi terlebih dahulu");
  } else {
    var url = baseURI + "_backyard/validate";
    if (!isLogin) {
      isLogin = true;
      ajaxRequestAsync(
        "post",
        url,
        "json",
        "application/json; charset=utf-8",
        JSON.stringify(voData),
        callBack,
        true
      );
    }
  }
}

$("#Username").keydown(function (e) {
  if (e.keyCode == 13) {
    if ($("#Username").val() != "") {
      $("#Password").focus();
    }
  }
});
$("#Password").keydown(function (e) {
  if (e.keyCode == 13) {
    if ($("#Password").val() != "") {
      processLogin();
    }
  }
});

function callBack(response) {
  isLogin = false;
  console.log(response);
  console.log(baseURI + "Home");
  if (response.errorcode == 0) {
    alertInfo("Login Success");
    setTimeout(function () {
      window.location.replace(baseURI + "Home");
    }, 1000);
  } else {
    isAlert("Error", response.msg);
  }
}
