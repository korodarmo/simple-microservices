var tableMaster;
var MenuID = "N12P2";

function initialize_table() {

    tableMaster = $("#tableMaster").DataTable({
        "processing": true,
        "ordering": true,
        "serverSide": true,
        "responsive": true,
        "searching": false,
        "lengthChange": false,
        "scrollX": true,
        "iDisplayLength": 25,
        "oLanguage": {
            "sEmptyTable": 'Data tidak di temukan'
        },
        "order": [[1, "asc"]],
        "ajax": {
            url: baseURI + "Query/CrudBasic/getDataTables",
            type: 'POST',
            data: function (data) {
                data.advSearch = objectifyForm($('#search-form-master').serializeArray());
            },
            statusCode: {
                401: function (xhr, error, thrown) {
                    window.location = window.location.origin + '/authentication/?next=' + window.location.pathname;
                    return false
                },
            },
            error: function (xhr) {
                $('#tableMaster_processing').css('display', 'none');
                $('#tableMaster').append('<tbody class="dataTables_empty"><tr><th colspan="99" style="text-align: center;">' + xhr?.responseJSON?.error + '</th></tr></tbody>');
            }
        },
        "columns": [
            {
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "description", "width": '75%' },
            {
                "orderable": false, "width": '15%',
                "data": "skillID",
                "render": function (data, type, row, meta) {
                    var result = '<div class="row">';
                    if (checkAkses(MenuID, 2)) {
                        result += '<div class="col-md-5"><button class="d-flex justify-content-center btn btn-green btn-xs btn-block edit_btn"><i class="icon-pencil edit-icon"></i> Edit</button></div>&nbsp;';
                    }
                    if (checkAkses(MenuID, 3)) {
                        result += '<div class="col-md-5"><button onclick="deleteData(' + data + ')"  class="d-flex justify-content-center btn btn-red btn-xs btn-block"><i class="icon-trash del-icon"></i> Delete</button></div>';
                    }
                    result += '</div>';
                    return result
                } }
        ]
    });


    $("#search-form-master input").change(function () {
        var c = $(this);
        $.when(
            c.blur()).then(function () {
                tableMaster.ajax.reload();
            });
    });

    $("#search-form-master input").on("keyup", function (e) {
        if (e.key === "Enter" || e.keyCode === 13) {
            tableMaster.ajax.reload();
        }
    });

    $('#search-form-master').submit(function () {
        return false;
    });

    

    $('#startedAt').datetimepicker({
        format: 'HH:mm',
        defaultDate: new Date()
    });

    $('#endedAt').datetimepicker({
        format: 'HH:mm',
        defaultDate: new Date()
    });

    $("#startedAt").on("dp.change", function (e) {
        CalcDiff()
    });

    $("#endedAt").on("dp.change", function (e) {
        CalcDiff()
    });
    closeForm();
}
function submitData() {
    if ($("#Description").val() == "" || !$("#Description").val().trim()) {
        isAlert("Skill Name Harus Diisi Lebih Dahulu..");
    } else {
        var SkillID = $('#SkillID').val() == "" ? "1" : $('#SkillID').val();
        var voData = {
            action: $('#typeForm').val(),
            SkillID: SkillID,
            Description: $('#Description').val(),
        };
        var url = baseURI + "Query/CrudBasic/prosessData";
        ajaxRequestAsync('post', url, 'json', 'application/json; charset=utf-8', JSON.stringify(voData), callBack, true);
    }
}

$('#tableMaster tbody').on('click', '.edit_btn', function () {
    $("#exampleModalLabel").text("Edit Skill Form");
    var data_row = tableMaster.row($(this).parents('tr')).data();
    console.log(data_row);
    $("#typeForm").val("edit");
    $('#SkillID').val(data_row.skillID);
    $('#Description').val(data_row.description);
    $('#modalForm').modal('show');
});

function createForm() {
    $("#typeForm").val("create");
    $("#exampleModalLabel").text("Add Skill Form");
    $('#modalForm').modal('show');
}

function closeForm() {
    $(".formInputText").val("");
    $('#modalForm').modal('hide');
}

$('#modalForm').on('hidden.bs.modal', function () {
    closeForm();
});

function deleteData(id) {
    alertify.confirm("Apakah Anda Yakin Ingin Menghapus Data Yang Terpilih ?", function (e) {
        if (e) {
            setTimeout(function () {
                var voData = {
                    action: 'delete',
                    SkillID: id
                };
                var url = baseURI + "Query/CrudBasic/prosessData";
                ajaxRequestAsync('post', url, 'json', 'application/json; charset=utf-8', JSON.stringify(voData), callBack, true);
            }, 750);
        }
    });
}

function callBack(response) {
    if (response.errorcode == 0) {
        alertSuccess(response.msg);
        setTimeout(function () {
            tableMaster.ajax.reload();
            closeForm();
        }, 1000);
    } else {
        isAlert("Error", response.msg);
    }
}
$(document).ready(function () {
    initialize_table();
});