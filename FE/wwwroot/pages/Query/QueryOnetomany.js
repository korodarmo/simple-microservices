var tableMaster;
var MenuID = "N12P2";

function initialize_table() {

    tableMaster = $("#tableMaster").DataTable({
        "processing": true,
        "ordering": true,
        "serverSide": true,
        "responsive": true,
        "searching": false,
        "lengthChange": false,
        "scrollX": true,
        "iDisplayLength": 25,
        "oLanguage": {
            "sEmptyTable": 'Data tidak di temukan'
        },
        "order": [[1, "asc"]],
        "ajax": {
            url: baseURI + "Query/Onetomany/getDataTables",
            type: 'POST',
            data: function (data) {
                data.advSearch = objectifyForm($('#search-form-master').serializeArray());
            },
            statusCode: {
                401: function (xhr, error, thrown) {
                    window.location = window.location.origin + '/authentication/?next=' + window.location.pathname;
                    return false
                },
            },
            error: function (xhr) {
                $('#tableMaster_processing').css('display', 'none');
                $('#tableMaster').append('<tbody class="dataTables_empty"><tr><th colspan="99" style="text-align: center;">' + xhr?.responseJSON?.error + '</th></tr></tbody>');
            }
        },
        "columns": [
            {
                "orderable": false,
                "render": function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { "data": "type" },
            { "data": "address" },
            { "data": "firstName" },
            { "data": "lastName" },
            {
                "orderable": false, "width": '15%',
                "data": "addressID",
                "render": function (data, type, row, meta) {
                    var result = '<div class="row">';
                    if (checkAkses(MenuID, 2)) {
                        result += '<div class="col-md-5"><button class="d-flex justify-content-center btn btn-green btn-xs btn-block edit_btn"><i class="icon-pencil edit-icon"></i> Edit</button></div>&nbsp;';
                    }
                    if (checkAkses(MenuID, 3)) {
                        result += '<div class="col-md-5"><button onclick="deleteData(' + data + ')"  class="d-flex justify-content-center btn btn-red btn-xs btn-block"><i class="icon-trash del-icon"></i> Delete</button></div>';
                    }
                    result += '</div>';
                    return result
                } }
        ]
    });


    $("#search-form-master input").change(function () {
        var c = $(this);
        $.when(
            c.blur()).then(function () {
                tableMaster.ajax.reload();
            });
    });

    $("#search-form-master input").on("keyup", function (e) {
        if (e.key === "Enter" || e.keyCode === 13) {
            tableMaster.ajax.reload();
        }
    });

    $('#search-form-master').submit(function () {
        return false;
    });

    $('#EmployeeID').select2({
        width: '100%',
        placeholder: 'Pilih Data Employee',
        ajax: {
            url: baseURI + 'Query/Onetoone/list',
            dataType: 'JSON',
            delay: 0,
            processResults: function (data, params) {
                var output = [];
                if (data.data) {
                    $.each(data.data, function (index, datas) {
                        output.push({
                            'id': datas.employeeID,
                            'text': datas.firstName + " " + datas.lastName
                        });
                    });
                }
                return {
                    results: output
                };
            }
        }
    });

    closeForm();
}
function submitData() {
    if ($("#EmployeeID").val() == "" || $("#EmployeeID").val() == null) {
        isAlert("Employee Harus Dipilih Lebih Dahulu..");
    } else if ($("#Address").val() == "" || !$("#Address").val().trim()) {
        isAlert("Address Harus Diisi Lebih Dahulu..");
    } else {
        var AddressID = $('#AddressID').val() == "" ? "1" : $('#AddressID').val();
        var voData = {
            action: $('#typeForm').val(),
            AddressID: AddressID,
            EmployeeID: $("#EmployeeID option:selected").val(),
            Type: $("#Type option:selected").val(),
            Address: $('#Address').val(),
        };
        var url = baseURI + "Query/Onetomany/prosessData";
        ajaxRequestAsync('post', url, 'json', 'application/json; charset=utf-8', JSON.stringify(voData), callBack, true);
    }
}

$('#tableMaster tbody').on('click', '.edit_btn', function () {
    $("#exampleModalLabel").text("Edit Address Employee Form");
    var data_row = tableMaster.row($(this).parents('tr')).data();
    $("#typeForm").val("edit");
    $('#AddressID').val(data_row.addressID);
    $('#EmployeeID').append("<option value='" + data_row.employeeID + "'>" + data_row.firstName + " " + data_row.lastName + "</option>");
    $('#EmployeeID').trigger('change');
    $('#Type').attr('disabled', true);
    $('#Type').val(data_row.type);
    $('#Address').val(data_row.address);
    $('#modalForm').modal('show');
});

function createForm() {
    $('#Type').attr('disabled', false);
    $("#typeForm").val("create");
    $("#exampleModalLabel").text("Add Address Employee Form");
    $('#modalForm').modal('show');
}

function closeForm() {
    $(".formInputSelect").empty().trigger("change");
    $(".formInputText").val("");
    $('#modalForm').modal('hide');
}

$('#modalForm').on('hidden.bs.modal', function () {
    closeForm();
});

function deleteData(id) {
    alertify.confirm("Apakah Anda Yakin Ingin Menghapus Data Yang Terpilih ?", function (e) {
        if (e) {
            setTimeout(function () {
                var voData = {
                    action: 'delete',
                    AddressID: id
                };
                var url = baseURI + "Query/Onetomany/prosessData";
                ajaxRequestAsync('post', url, 'json', 'application/json; charset=utf-8', JSON.stringify(voData), callBack, true);
            }, 750);
        }
    });
}

function callBack(response) {
    if (response.errorcode == 0) {
        alertSuccess(response.msg);
        setTimeout(function () {
            tableMaster.ajax.reload();
            closeForm();
        }, 1000);
    } else {
        isAlert("Error", response.msg);
    }
}
$(document).ready(function () {
    initialize_table();
});