
function goBack(URL) {
    if ($.fn.lsClear) {
        lsClear();
    }
    location.href = URL;
}
function nospacesInput(t) {
    if (t.value.match(/\s/g)) {
        t.value = t.value.replace(/\s/g, '');
    }
    if (t.keyCode == 32) {
        return false;
    }
}
function nospacesKey(e) {
    if (e.which === 32)
        return false;
}

function alertSuccess(title) {
    alertify.set({ delay: 4000 });
    alertify.success(title);
}
function alertInfo(title) {
    alertify.set({ delay: 4000 });
    alertify.log(title);
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
            return '<span class="' + cls + '">' + match + '</span>';
    });
}

$(document).ready(function () {
    $('.dateSearch').datepicker({
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months",
        offset: -1,
        autoPick: true
    });
});
