var formActionGutter = 25;
var monthsArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var idleUse = $("body").hasClass("usingIdle");
var idleIsLocked = false;
var idleTimer = 900000;// ms
var defprom;

var baseURI = '';
var slug = '';
var env = 'Development';

if (slug == '') {
    slug = '/';
} else {
    slug = '/' + slug + '/';
}

baseURI = window.location.origin + slug;

$(document).ready(function () {

    var scrollTop = $(window).scrollTop();
    setShadowNavbar(scrollTop);
    $(document).on('pjax:send', function () {
        startLoading();
    });

    $(document).on('pjax:complete', function (e, x, z) {
        endLoading();
    });
});

$(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    setShadowNavbar(scrollTop);
});

function setShadowNavbar(scrollTop) {
    if (scrollTop > 180) {
        $(".navbar.fixed-top").addClass('shadows');
    } else {
        $(".navbar.fixed-top").removeClass('shadows');
    }
}

function ajaxRequestAsync(method, url, dataType, contentType, data, callback, isLoading) {
    if (dataType !== 'formData') {
        $.ajax({
            type: method,
            url: url,
            data: data,
            dataType: dataType,
            contentType: contentType,
            async: true,
            headers: { 'dataType': dataType },
            beforeSend: function (xhr) {
                if (isLoading) {
                    startLoading();
                }
            },
            success: callback,
            error: responseError,
        }).done(function (data) {
            if (isLoading) {
                endLoading();
            }
        });
    } else {
        $.ajax({
            type: method,
            url: url,
            data: data,
            async: true,
            processData: false,
            beforeSend: function (xhr) {
                if (isLoading) {
                    startLoading();
                }
            },
            contentType: false,
            headers: { 'dataType': dataType },
            success: callback,
            error: responseError,
        }).done(function (data) {
            if (isLoading) {
                endLoading();
            }
        });
    }
}

function responseError(xhr) {
    endLoading();

    switch (xhr.status) {
        case 403:
            isAlert('Error 403', 'Forbidden Access');
            break;
        default:
            if (typeof xhr.responseJSON != 'undefined') {
                message = xhr.responseJSON['message']
            } else {
                if (xhr.responseText != '') {
                    message = xhr.responseText;
                } else {
                    message = xhr.statusText;
                }
            }
            if (env === 'Development') {
                isAlert('Error ' + xhr.status, message);
            } else {
                isAlert('Error ' + xhr.status, "Something is broken here. But it's not your fault.");
            }


            break;
    }
}

function startLoading() {
    $.blockUI({
        message: '<i class="icon ion-load-a spinner mr-2"></i> Loading...',
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
    $('.blockUI').css({
        'z-index': 9999999
    });
}

function endLoading() {
    $(".blockUI").remove();
}

function objectifyForm(formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}


function isAlert(title, content) {
    if (content === undefined) {
        alertError(title);
    } else {
        alertError(title + "<br>" + content);
    }
}


function alertError(title) {
    alertify.set({ delay: 4000 });
    alertify.error(title);
}

window.onerror = function (msg, url, line, col, error) {
    var extra = !col ? '' : '\ncolumn: ' + col;
    extra += !error ? '' : '\nerror: ' + error;

    if (env == 'Development')
        isAlert("Error: " + msg + "\nurl: " + url + "\nline: " + line + extra);

    // TODO: Report this error via ajax so you can keep track
    // of what pages have JS issues

    var suppressErrorAlert = true;
    return suppressErrorAlert;
};

function converterToDateTime(dt) {
    return dt.getDate() + " " +
        monthsArr[dt.getMonth()] + " " +
        dt.getFullYear() + "   " +
        (dt.getHours() < 10 ? '0' : '') + dt.getHours() + ":" +
        (dt.getMinutes() < 10 ? '0' : '') + dt.getMinutes() + ":" +
        (dt.getSeconds() < 10 ? '0' : '') + dt.getSeconds();
}

function converterToDate(dt) {
    return dt.getDate() + " " +
        monthsArr[dt.getMonth()] + " " +
        dt.getFullYear();
}

$(".sidenav-item").on("click", function () {
    $('.sidenav-item').removeClass('active');
    $(this).addClass('active');
})
