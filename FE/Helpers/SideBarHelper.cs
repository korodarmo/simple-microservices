﻿using ePM_frontend.Helpers.Database;
using ePM_frontend.Services.System.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ePM_frontend.Helpers
{
    public class SideBarHelper
    {
        public static string IsActiveDropDownMenu(ViewContext Context, List<string> controller)
        {
            var curController = Context.RouteData.Values["Controller"].ToString().ToLower();

            if (controller.Any(x => x.ToLower().Trim() == curController))
            {
                return "active open";
            }
            else
            {
                return "";
            }
        }

        public static string IsActiveSingleMenu(ViewContext Context, string controller, string action)
        {
            var curController = Context.RouteData.Values["Controller"].ToString().ToLower();
            var curAction = Context.RouteData.Values["Action"].ToString().ToLower();

            if (controller.ToLower().Trim() == curController && action.ToLower().Trim() == curAction)
            {
                return "active";
            }
            else
            {
                return "";
            }
        }

        public static string IsActiveSingleMenu(ViewContext Context, string controller)
        {
            var curController = Context.RouteData.Values["Controller"].ToString().ToLower();

            if (controller.ToLower().Trim() == curController)
            {
                return "active";
            }
            else
            {
                return "";
            }
        }

        public static List<MenuModel> getParentMenu()
        {
            SqlContext _SqlContext = new SqlContext();
            List<MenuModel> returnVal = new List<MenuModel>();
            try
            {
                var sql = $@" SELECT * FROM {TableEnum.s_menu.ToString()} where RecordFlag = 1 and Status = 1  ORDER BY Ordering ASC";
                returnVal = ActionDb.getAllData<MenuModel>(_SqlContext, sql, null);
                if (returnVal == null)
                {
                    returnVal = new List<MenuModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<MenuModel>();
            }

            return returnVal;
        }

        public static List<MenuModel> getListChildMenu(string MenuID)
        {
            SqlContext _SqlContext = new SqlContext();
            List<MenuModel> returnVal = new List<MenuModel>();
            try
            {
                var sql = $@" SELECT * FROM {TableEnum.s_menu.ToString()} where MenuID LIKE '%{MenuID}%' and RecordFlag = 0 and Status = 1 and Ordering > 0 ORDER BY Ordering ASC";
                returnVal = ActionDb.getAllData<MenuModel>(_SqlContext, sql, null);
                if (returnVal == null)
                {
                    returnVal = new List<MenuModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<MenuModel>();
            }

            return returnVal;
        }
    }
}
