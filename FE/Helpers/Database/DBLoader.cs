using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO; 
using System.Collections;
using System;

namespace ePM_frontend.Helper.Database
{
    public class DBLoader
    {
        public static string GetConnstring()
        {
            string Server = Environment.GetEnvironmentVariable("DB_SERVER");
            string Db = Environment.GetEnvironmentVariable("DB_NAME");
            string User = Environment.GetEnvironmentVariable("DB_USER");
            string Password = Environment.GetEnvironmentVariable("DB_PASS");
            string Connstring = $@"Data Source={Server};Initial Catalog={Db};Persist Security Info=True;User ID={User};Password={Password};TrustServerCertificate=True";
            return Connstring;
        }
    }
}
