﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO;
using System;

namespace ePM_frontend.Helper.Database
{
    public class HostLoader
    {
        public static string GetHostUrl()
        {
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            // var MysqlConnstring = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("OneKalbeSettings");
            
            // return MysqlConnstring["OneKalbe.Host"];

            var MysqlConnstring = Environment.GetEnvironmentVariable("ONE_KALBE_HOST");
            Console.WriteLine("GetHostUrl: " + MysqlConnstring);
            return MysqlConnstring;
        }

        public static string GetHostSso()
        {
            var FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "")).Root;

            // var MysqlConnstring = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("Kalbe");
            
            // return MysqlConnstring["OneKalbeSSO"];
            var MysqlConnstring = Environment.GetEnvironmentVariable("ONE_KALBE_SSO");
            Console.WriteLine("GetHostSso: " + MysqlConnstring);
            return MysqlConnstring;
        }
    }
}
