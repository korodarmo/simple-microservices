﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ePM_frontend.Helper.Database
{
    public class MssqlContext
    {
        public string ConnectionString { get; set; }

        public MssqlContext()
        {
            string connectionString = DBLoader.GetConnstring();
            this.ConnectionString = connectionString;
        }

        private SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}
