﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ePM_frontend.Helpers.Database
{
    public class ActionDb
    {
        public static long insertData(SqlContext _context, string sql, DynamicParameters param, CommandType commandType = CommandType.Text)
        {
            long result = 0;
            using IDbConnection db = _context.GetConnection();
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    var lastInsertedId = db.Query<long>(sql, param, commandType: commandType, transaction: tran).FirstOrDefault();
                    if (lastInsertedId > 0)
                    {
                        result = Convert.ToInt64(lastInsertedId);
                    }
                    else
                    {
                        throw new Exception("Insert Data Fail");
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }
            return result;
        }
        public static T updateData<T>(SqlContext _context, string sql, DynamicParameters param, CommandType commandType = CommandType.Text)
        {
            T result;
            using IDbConnection db = _context.GetConnection();
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    result = db.Query<T>(sql, param, commandType: commandType, transaction: tran).FirstOrDefault();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;
        }
        public static T getData<T>(SqlContext _context, string sqlCommand, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            return db.Query<T>(sqlCommand, parms, commandType: commandType).FirstOrDefault();
        }
        public static List<T> getAllData<T>(SqlContext _context, string sqlCommand, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            return db.Query<T>(sqlCommand, parms, commandType: commandType).ToList();
        }
    }
}
