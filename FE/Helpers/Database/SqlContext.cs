﻿using ePM_frontend.Helper.Database;
using Microsoft.Data.SqlClient;

namespace ePM_frontend.Helpers.Database
{
    public class SqlContext
    {
        public string ConnectionString { get; set; }

        public SqlContext()
        {
            string connectionString = DBLoader.GetConnstring();
            this.ConnectionString = connectionString;
        }

        public SqlConnection GetConnection() { return new SqlConnection(ConnectionString); }
    }
}
