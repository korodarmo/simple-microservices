﻿namespace ePM_frontend.Helpers
{
    public class BaseResponse
    {
        public bool status { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public int row { get; set; }
    }
    public class BaseResponseEbridging
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }
    public class AuditTrailsResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}