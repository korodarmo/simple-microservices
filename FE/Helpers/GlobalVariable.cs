﻿using System;

namespace ePM_frontend.Helpers
{
    public static class GlobalVariable
    {
        public static DateTime ExpiredTokenTime { get; set; }
        public static string CurrentToken { get; set; }
        

        public static string notHaveAkses = "Anda Tidak Memiliki Hak Akses untuk melakukan proses ini";
        public static string exist = " Sudah Terdaftar dalam database";
        public static string prosesSuccess = "Process Berhasil Dilakukan";
        public static string prosesFail = "Process Tidak Berhasil Dilakukan";
        public static string notfound = "Data Tidak Dapat Ditemukan";
        public static string serviceNotAvailabel = "Mohon maaf service tidak tersedia atau sedang offline...";
    }
}