﻿using System.Threading.Tasks;

namespace ePM_frontend.Helper.Email
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
