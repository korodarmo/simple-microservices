﻿using System.ComponentModel.DataAnnotations;

namespace ePM_frontend.Helper.Email
{
    public class Kontak
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Message { get; set; }
    }
}
