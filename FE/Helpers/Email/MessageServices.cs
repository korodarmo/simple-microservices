﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;

namespace ePM_frontend.Helper.Email
{
    public class AuthMessageSender : IEmailSender
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("KALBE FARMA", "no-replay@kalbefarma.id"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart("html") { Text = message };

            await Task.Run(() =>
            {
                using (var client = new SmtpClient())
                {
                    client.Connect("smtp.hostinger.co.id", 587, false);

                    client.Authenticate("no-replay@kalbefarma.id", "PasswordDisini");

                    client.Send(emailMessage);
                    client.Disconnect(true);
                }
            });
        }
    }
}