﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;

namespace ePM_frontend.Helpers
{
    public class GlobalFunction
    {
        public static string CreateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static List<string> converListAuth(string AuthAcces)
        {
            List<string> returnList = new List<string>();
            if (!String.IsNullOrEmpty(AuthAcces))
            {
                returnList = AuthAcces.Split('|').ToList();
            }
            return returnList;
        }

        public static bool CheckHakAkses(HttpContext httpContext, string menuID, string act)
        {
            var action = new Dictionary<string, int> {
                { "View", 0 }, { "Add", 1 }, { "Update", 2 }, { "Delete", 3 }, { "Other", 4 }, { "Post", 5 }, { "Unpost", 6 }, { "Export", 7 }
            };
            int getActionKey = action[act];
            bool status = false;
            string roleAccess = httpContext.Session.GetString("RoleAccess");
            List<string> listAuth = converListAuth(roleAccess);
            string exist = listAuth.Find(f => f.Contains(menuID));
            if (!string.IsNullOrEmpty(exist))
            {
                char[] value = exist.Split('=')[1].ToCharArray();
                if (value[getActionKey].ToString() == "1")
                {
                    status = true;
                }
            }
            return status;
        }
    }
}