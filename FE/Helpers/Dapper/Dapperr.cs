﻿using Dapper;
using Microsoft.Extensions.Configuration;
using ePM_frontend.Helpers.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace ePM_frontend.Helpers.Dapper
{
    public class Dapperr : IDapper
    {
        private readonly IConfiguration _config;
        SqlContext _context;

        public Dapperr(IConfiguration config)
        {
            _config = config;
            _context = new SqlContext();
        }
        public void Dispose() { }

        public int Execute(string sqlCommand, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            int result;
            using IDbConnection db = _context.GetConnection();
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    result = db.Execute(sqlCommand, parms, commandType: commandType, transaction: tran);
                    tran.Commit();
                    Console.WriteLine($"Affected Rows: {result}");

                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;

        }
        public bool ExistData<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            dynamic x = db.Query<T>(sqlCommand, parms, commandType: commandType).SingleOrDefault(); ;
            if(x.total > 0)
            {
                return true;
            }
            return false;
        }
        public int CountAll(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            return db.Query<int>(sqlCommand, parms, commandType: commandType).Count();
        }
        public int CountAll<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            dynamic x = db.Query<T>(sqlCommand, parms, commandType: commandType).SingleOrDefault(); ;
            return x.total;
        }
        public T Get<T>(string sqlCommand, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            return db.Query<T>(sqlCommand, parms, commandType: commandType).FirstOrDefault();
        }

        public List<T> GetAll<T>(string sqlCommand, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            using IDbConnection db = _context.GetConnection();
            return db.Query<T>(sqlCommand, parms, commandType: commandType).ToList();
        }

        public long Insert(string sp, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            long result = 0;
            using IDbConnection db = _context.GetConnection();
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    var lastInsertedId = db.Query<long>(sp, parms, commandType: commandType, transaction: tran).FirstOrDefault();
                    if (lastInsertedId > 0)
                    {
                        result = Convert.ToInt64(lastInsertedId);
                    }
                    else
                    {
                        throw new Exception("Insert Data Fail");
                    }
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;
        }
        public T Insert2<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            T result;
            using IDbConnection db = _context.GetConnection();
            Console.WriteLine(sp);
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    result = db.Query<T>(sp, parms, commandType: commandType, transaction: tran).FirstOrDefault();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;
        }

        public T Update<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            T result;
            using IDbConnection db = _context.GetConnection();
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    result = db.Query<T>(sp, parms, commandType: commandType, transaction: tran).FirstOrDefault();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;
        }

        public T Delete<T>(string sp, DynamicParameters parms, CommandType commandType = CommandType.Text)
        {
            T result;
            using IDbConnection db = _context.GetConnection();
            try
            {
                if (db.State == ConnectionState.Closed)
                    db.Open();

                using var tran = db.BeginTransaction();
                try
                {
                    result = db.Query<T>(sp, parms, commandType: commandType, transaction: tran).FirstOrDefault();
                    tran.Commit();
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (db.State == ConnectionState.Open)
                    db.Close();
            }

            return result;
        }
    }
}
