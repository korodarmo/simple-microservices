﻿using ePM_frontend.Helper;
using ePM_frontend.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace ePM_frontend.Helpers
{
    public class AuditTrails
    {
        public static bool PushData<T>(T poAfter, T poBefore, AuditTrailsModel log)
        {
            try
            {
                string After = "";
                string Before = "";
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(T));
                if (poAfter != null)
                {
                    MemoryStream afObj = new MemoryStream();
                    js.WriteObject(afObj, poAfter);
                    afObj.Position = 0;
                    StreamReader srAfter = new StreamReader(afObj);
                    After = srAfter.ReadToEnd();
                }
                else { After = "Empty"; }

                if (poBefore != null)
                {
                    MemoryStream bfObj = new MemoryStream();
                    js.WriteObject(bfObj, poBefore);
                    bfObj.Position = 0;
                    StreamReader srBefore = new StreamReader(bfObj);
                    Before = srBefore.ReadToEnd();
                }
                else { Before = "Empty"; }
 
                var url = Environment.GetEnvironmentVariable("EAUD_BASE_URL") + Environment.GetEnvironmentVariable("EAUD_PUSH_DATA");
                string appKeys = Environment.GetEnvironmentVariable("EPM_KEY");
                var client = new RestClient(url);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-type", "application/json");

                AuditTrailsModel newLog = new AuditTrailsModel();
                newLog = log;
                newLog.AppKey = appKeys;
                newLog.Source = "EPM-FrontEnd";
                newLog.ValueOld = Before;
                newLog.ValueNew = After;

                request.AddJsonBody(newLog);
                IRestResponse response = client.Execute(request);
                var deserializeResponse = JsonConvert.DeserializeObject<AuditTrailsResponse>(response.Content);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
