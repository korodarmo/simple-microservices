﻿using Microsoft.AspNetCore.Http;

namespace ePM_frontend.Helpers
{
    public class BaseHeader
    {
        public class BaseHeaderModel
        {
            public string UserID { get; set; }
            public string Name { get; set; }
            public string Username { get; set; }
            public string RoleAccess { get; set; }
            public string RoleID { get; set; }
            public string Rolename { get; set; }
            public string Instance { get; set; }
            public int OrganizationID { get; set; }
            public string OrganizationName { get; set; }
            public string IpAddress { get; set; }
            public string Line { get; set; }
            public string Competency { get; set; }
        }
        public static BaseHeaderModel getBaseHeader(HttpContext httpContext)
        {
            BaseHeaderModel header = new BaseHeaderModel();
            header.UserID = httpContext.Session.GetString("UserID");
            header.Name = httpContext.Session.GetString("Name");
            header.Username = httpContext.Session.GetString("Username");
            header.RoleAccess = httpContext.Session.GetString("RoleAccess");
            header.RoleID = httpContext.Session.GetString("RoleID");
            header.Rolename = httpContext.Session.GetString("Rolename");
            header.Instance = httpContext.Session.GetString("Instance");
            header.Instance = httpContext.Session.GetString("Instance");
            header.OrganizationID = (int)httpContext.Session.GetInt32("OrganizationID");
            header.OrganizationName = httpContext.Session.GetString("OrganizationName");
            header.IpAddress = httpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            header.Line = httpContext.Session.GetString("Line");
            header.Competency = httpContext.Session.GetString("Competency");
            return header;

        }
       

    }
}