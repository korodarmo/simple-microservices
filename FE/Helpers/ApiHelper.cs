﻿using Newtonsoft.Json;
using RestSharp;
using System;

namespace ePM_frontend.Helpers
{
    public class ApiHelper
    {
        public static RestRequest requestAddAnonymousHeader(RestRequest req)
        {

            req.AddHeader("Content-Type", "application/json");

            return req;
        }

        public static RestRequest requestAddAnonymousHeader(RestRequest req, object reqBodyObj)
        {

            req.AddHeader("Content-Type", "application/json");
            var reqBody = JsonConvert.SerializeObject(reqBodyObj);
            req.AddParameter("application/json",
                            reqBody,
                            ParameterType.RequestBody);
            return req;
        }

        public static RestRequest requestAddStandardHeader(RestRequest req)
        {

            req.AddHeader("Content-Type", "application/json");
            req.AddHeader("Authorization", $"Bearer {GlobalVariable.CurrentToken}");

            return req;
        }
        public static RestRequest requestAddStandardHeaderWithBody(RestRequest req, object reqBodyObj)
        {
            req.AddHeader("Content-Type", "application/json");
            req.AddHeader("Authorization", $"Bearer {GlobalVariable.CurrentToken}");
            var reqBody = JsonConvert.SerializeObject(reqBodyObj);
            req.AddParameter("application/json",
                            reqBody,
                            ParameterType.RequestBody);
            return req;
        }
    }
}