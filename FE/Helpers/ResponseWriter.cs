﻿using ePM_frontend.Helper.Database;
using System.IO;

namespace ePM_frontend.Helpers
{
    public class ResponseWriter
    {
        public static void responseWriter(string fileName, string jsonValue)
        {
            TextWriter writer;
            using (writer = new StreamWriter(DbLocalInitial.configPath + fileName + ".json", append: false))
            {
                writer.WriteLine(jsonValue);
            }
        }
    }
}
