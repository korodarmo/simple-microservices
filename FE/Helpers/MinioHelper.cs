﻿using Newtonsoft.Json;
using RestSharp;
using System;
using Minio;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.StaticFiles;
using System.IO;
using Microsoft.AspNetCore.Http;
using ePM_frontend.Models;
using ePM_frontend.Helpers;

namespace ePM_frontend.Helper
{
    public class MinioHelper
    {
        private static MinioClient minio;
        public MinioHelper()
        {
        }
        public static async Task<MinioModel> GetFileBase64(string filename, string original_file_name, string type)
        {  
            string endpoint = Environment.GetEnvironmentVariable("MINIO_ENPOINT");
            string accessKey = Environment.GetEnvironmentVariable("MINIO_ACCESS_KEY");
            string secretKey = Environment.GetEnvironmentVariable("MINIO_SECRET_KEY");
            string bucketName = Environment.GetEnvironmentVariable("MINIO_BUCKET_NAME");
            string location = Environment.GetEnvironmentVariable("MINIO_LOCATION");

            var result = new MinioModel();
            try
            {

                if (string.IsNullOrEmpty(filename))
                {
                    result.message = "Filename diperlukan";
                    return result;
                }

                if (string.IsNullOrEmpty(original_file_name))
                {
                    result.message = "Nama file Asli diperlukan";
                    return result;
                }

                if (string.IsNullOrEmpty(type))
                {
                    result.message = "Tipe file diperlukan";
                    return result;
                }

                var curType = GetMinioFolderType(type);

                if (string.IsNullOrEmpty(curType))
                {
                    result.message = $"Tipe file tidak dikenal : {curType}";
                    return result;
                }

                var full_path_file = filename;
                if (!string.IsNullOrEmpty(curType))
                {
                    full_path_file = string.Join("", "/", curType, "/", filename);
                }

                minio = new MinioClient()
                                    .WithEndpoint(endpoint)
                                    .WithCredentials(accessKey,secretKey)
                                    .WithSSL()
                                    .Build();

                var found = minio.BucketExistsAsync(bucketName)?.Result;

                if (found != true)
                {
                    minio.MakeBucketAsync(bucketName, location);
                }

                StatObjectArgs statObjectArgs = new StatObjectArgs()
                                                         .WithBucket(bucketName)
                                                         .WithObject(full_path_file);
                var test = await minio.StatObjectAsync(statObjectArgs);

                var memory = new MemoryStream();

                GetObjectArgs getObjectArgs = new GetObjectArgs()
                                            .WithBucket(bucketName)
                                            .WithObject(full_path_file)
                                            .WithCallbackStream((stream) =>
                                            {
                                                stream.CopyTo(memory);
                                            });
                await minio.GetObjectAsync(getObjectArgs);
                memory.Position = 0;

                var provider = new FileExtensionContentTypeProvider();
                string contentType;

                if (!provider.TryGetContentType(original_file_name, out contentType))
                {
                    contentType = "application/octet-stream";
                }

                result.succeeded = true;
                result.data = memory?.ToArray();
                result.new_file_name = filename;
                result.original_file_name = original_file_name;
                result.full_path_new_file = full_path_file;
                result.content_type = contentType;
            }
            catch (Exception ex)
            {
                result.message = "File tidak ditemukan";
            }

            return result;
        }

        public static async Task<MinioModel> UploadFileStream(Stream stream, string original_file_name, string type)
        {
            string endpoint = Environment.GetEnvironmentVariable("MINIO_ENPOINT");
            string accessKey = Environment.GetEnvironmentVariable("MINIO_ACCESS_KEY");
            string secretKey = Environment.GetEnvironmentVariable("MINIO_SECRET_KEY");
            string bucketName = Environment.GetEnvironmentVariable("MINIO_BUCKET_NAME");
            string location = Environment.GetEnvironmentVariable("MINIO_LOCATION");

            var result = new MinioModel();
            try
            {
                if (stream == null)
                {
                    result.message = "File stream diperlukan";
                    return result;
                }

                if (string.IsNullOrEmpty(type))
                {
                    result.message = "Tipe file harus diisi";
                    return result;
                }

                var curType = GetMinioFolderType(type);
                if (string.IsNullOrEmpty(curType))
                {
                    result.message = $"Tipe file tidak dikenal : {curType}";
                    return result;
                }

                var new_file_name = string.Join("", Path.GetFileNameWithoutExtension(original_file_name), "_", DateTime.Now.ToString("yyyy_MM_dd__hh_mm_ss"), "_", Path.GetExtension(original_file_name));

                var full_path_new_file = new_file_name;
                if (!string.IsNullOrEmpty(curType))
                {
                    full_path_new_file = string.Join("", "/", curType, "/", new_file_name);
                }

                string contentType;
                var provider = new FileExtensionContentTypeProvider();

                if (!provider.TryGetContentType(original_file_name, out contentType))
                {
                    contentType = "application/octet-stream";
                }

                minio = new MinioClient()
                        .WithEndpoint(endpoint)
                        .WithCredentials(accessKey,secretKey)
                        //.WithSSL() //Nyalakan kalau pakai SSL
                        .Build();

                var found = minio.BucketExistsAsync(bucketName)?.Result;

                if (found != true)
                {
                    minio.MakeBucketAsync(bucketName, location);
                }

                stream.Position = 0;
                await minio.PutObjectAsync(bucketName, full_path_new_file, stream, stream.Length, contentType);

                result.succeeded = true;
                result.new_file_name = new_file_name;
                result.original_file_name = original_file_name;
                result.full_path_new_file = full_path_new_file;
                result.content_type = contentType;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Minio Error : " + ex.Message.ToString());
                result.message = "File tidak ditemukan";
            }
            return result;
        }

        public static async Task<string> GetFileUpload(string filename)
        {
            string endpoint = Environment.GetEnvironmentVariable("MINIO_ENPOINT");
            string accessKey = Environment.GetEnvironmentVariable("MINIO_ACCESS_KEY");
            string secretKey = Environment.GetEnvironmentVariable("MINIO_SECRET_KEY");
            string bucketName = Environment.GetEnvironmentVariable("MINIO_BUCKET_NAME");
            string location = Environment.GetEnvironmentVariable("MINIO_LOCATION");

            string url = "";
            try
            {
                minio = new MinioClient()
                        .WithEndpoint(endpoint)
                        .WithCredentials(accessKey,secretKey)
                        .WithSSL()
                        .Build();

                StatObjectArgs statObjectArgs = new StatObjectArgs().WithBucket(bucketName).WithObject(filename);
                var test = await minio.StatObjectAsync(statObjectArgs);

                PresignedGetObjectArgs args = new PresignedGetObjectArgs()
                                        .WithBucket(bucketName)
                                        .WithObject(filename)
                                        .WithExpiry(60 * 60 * 24);
                url = await minio.PresignedGetObjectAsync(args);
            }
            catch (Exception)
            {
                url = "";
            }
            return url;
        }

        public static string GetMinioFolderType(string type)
        {
            var curType = "";
            if (!string.IsNullOrEmpty(type))
            {
                type = type?.ToLower()?.Trim();
                switch (type)
                {
                    case "breakdown":
                        curType = "breakdown";
                        break;
                    case "preventive":
                        curType = "preventive";
                        break; 
                    case "guideline":
                        curType = "guideline";
                        break; 
                    default:
                        curType = "";
                        break;
                }
            }
            return curType;
        }
    }
}