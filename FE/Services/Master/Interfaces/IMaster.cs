﻿using ePM_frontend.Helpers;
using ePM_frontend.Models;
using ePM_frontend.Services.Master.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using static ePM_frontend.Helpers.BaseHeader;

namespace ePM_frontend.Services.Master.Interfaces
{
    public interface IMaster
    {
        Task<DataTableResultModel> getDataCrudBasic(DataTableBindingModel Args);
        Task<Result<SkillModel>> createCrudBasic(BaseHeaderModel baseHeaderModel, SkillModel payload);
        Task<Result<SkillModel>> updateCrudBasic(BaseHeaderModel baseHeaderModel, SkillModel payload);
        Task<Result<SkillModel>> deleteCrudBasic(BaseHeaderModel baseHeaderModel, SkillModel payload);
        Task<Result<List<SkillModel>>> getListCrudBasic(string filter);

        Task<DataTableResultModel> getDataOnetoone(DataTableBindingModel Args);
        Task<Result<EmployeeModel>> createOnetoone(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<EmployeeModel>> updateOnetoone(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<EmployeeModel>> deleteOnetoone(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<List<EmployeeModel>>> getListOnetoone(string filter);

        Task<DataTableResultModel> getDataOnetomany(DataTableBindingModel Args);
        Task<Result<EmployeeModel>> createOnetomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<EmployeeModel>> updateOnetomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<EmployeeModel>> deleteOnetomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload);

        Task<DataTableResultModel> getDataManytomany(DataTableBindingModel Args);
        Task<Result<EmployeeModel>> createManytomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<EmployeeModel>> updateManytomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
        Task<Result<EmployeeModel>> deleteManytomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload);
    }

}
