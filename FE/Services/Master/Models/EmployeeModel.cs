﻿using System;
using System.Text.Json.Serialization;

namespace ePM_frontend.Services.Master.Models
{
    public class EmployeeModel
    {
        public string action { get; set; }

        [JsonIgnore]
        public long EmployeeID { get; set; }
        [JsonIgnore]
        public DateTime RecordTime { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PassportNumber { get; set; }
        [JsonIgnore]
        public long EmpSID { get; set; }
        [JsonIgnore]
        public long SkillID { get; set; }
        [JsonIgnore]
        public long AddressID { get; set; }
        public string Type { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }

    }

}
