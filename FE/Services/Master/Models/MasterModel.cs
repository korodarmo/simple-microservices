﻿using System;
using System.Text.Json.Serialization;

namespace ePM_frontend.Services.Master.Models
{
    public class SkillModel
    {
        [JsonIgnore]
        public long SkillID { get; set; }
        public string Description { get; set; }
        public string action { get; set; }
    }

}
