using ePM_frontend.Helpers;
using ePM_frontend.Models;
using ePM_frontend.Services.Master.Interfaces;
using ePM_frontend.Services.Master.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using static ePM_frontend.Helpers.BaseHeader;

namespace ePM_frontend.Services.Master.DALS
{
    public class MasterDAL : ApiHelper, IMaster
    {
        private string pathUrlCrudBasic = "master/skill";
        private string pathUrlCrudOnetoone = "employee/onetoone";
        private string pathUrlCrudOnetomany = "employee/onetomany";
        private string pathUrlCrudManytomany = "employee/manytomany";

        #region 'Crud Basic'
        public async Task<DataTableResultModel> getDataCrudBasic(DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "Description";
                        break;

                    default:
                        break;
                }

                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudBasic + "/getDatatables" +
                    $"?pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder?.dir?.ToUpper()}");
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<SkillModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data != null ? deserializeResponse?.Data : new List<SkillModel>(),
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<SkillModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }
        public async Task<Result<SkillModel>> createCrudBasic(BaseHeaderModel baseHeaderModel, SkillModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudBasic);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<SkillModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<SkillModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<SkillModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<SkillModel>> updateCrudBasic(BaseHeaderModel baseHeaderModel, SkillModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudBasic + "/" + payload.SkillID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.PATCH);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<SkillModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<SkillModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<SkillModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<SkillModel>> deleteCrudBasic(BaseHeaderModel baseHeaderModel, SkillModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudBasic + "/" + payload.SkillID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.DELETE);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<SkillModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<SkillModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<SkillModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<List<SkillModel>>> getListCrudBasic(string filter)
        {
            try
            {
                string filtering = string.IsNullOrEmpty(filter) ? " " : "?term=" + filter;
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudBasic + "/listSkill" + filtering);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.GET);
                request = requestAddStandardHeader(request);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<Result<List<SkillModel>>>(response.Content);
                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<List<SkillModel>>()
                {
                    Message = ex.Message
                };
            }
        }
        #endregion

        #region 'Onetoone'
        public async Task<DataTableResultModel> getDataOnetoone(DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "emp.FirstName";
                        break;
                    case 2:
                        orderBy = "emp.LastName";
                        break;
                    case 3:
                        orderBy = "emp.Email";
                        break;
                    case 4:
                        orderBy = "emp.Phone";
                        break;
                    case 5:
                        orderBy = "emp_d.PassportNumber";
                        break;

                    default:
                        break;
                }

                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetoone + "/getDatatables" +
                    $"?pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder?.dir?.ToUpper()}");
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<EmployeeModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data != null ? deserializeResponse?.Data : new List<EmployeeModel>(),
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<EmployeeModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> createOnetoone(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetoone);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> updateOnetoone(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetoone + "/" + payload.EmployeeID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.PATCH);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> deleteOnetoone(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetoone + "/" + payload.EmployeeID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.DELETE);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<List<EmployeeModel>>> getListOnetoone(string filter)
        {
            try
            {
                string filtering = string.IsNullOrEmpty(filter) ? " " : "?term=" + filter;
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetoone + "/list" + filtering);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.GET);
                request = requestAddStandardHeader(request);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<Result<List<EmployeeModel>>>(response.Content);
                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<List<EmployeeModel>>()
                {
                    Message = ex.Message
                };
            }
        }
        #endregion

        #region 'Onetomany'
        public async Task<DataTableResultModel> getDataOnetomany(DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "emp_a.Type";
                        break;
                    case 2:
                        orderBy = "emp_a.Address";
                        break;
                    case 3:
                        orderBy = "emp.FirstName";
                        break;
                    case 4:
                        orderBy = "emp.LastName";
                        break;

                    default:
                        break;
                }

                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetomany + "/getDatatables" +
                    $"?pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder?.dir?.ToUpper()}");
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<EmployeeModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data != null ? deserializeResponse?.Data : new List<EmployeeModel>(),
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<EmployeeModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> createOnetomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetomany);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> updateOnetomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetomany + "/" + payload.AddressID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.PATCH);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> deleteOnetomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudOnetomany + "/" + payload.AddressID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.DELETE);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        #endregion

        #region 'Manytomany'
        public async Task<DataTableResultModel> getDataManytomany(DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "emp_sk.Description";
                        break;
                    case 2:
                        orderBy = "emp.FirstName";
                        break;
                    case 3:
                        orderBy = "emp.LastName";
                        break;

                    default:
                        break;
                }

                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudManytomany + "/getDatatables" +
                    $"?pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder?.dir?.ToUpper()}");
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<EmployeeModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data != null ? deserializeResponse?.Data : new List<EmployeeModel>(),
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<EmployeeModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> createManytomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudManytomany);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.POST);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> updateManytomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudManytomany + "/" + payload.EmpSID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.PATCH);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<EmployeeModel>> deleteManytomany(BaseHeaderModel baseHeaderModel, EmployeeModel payload)
        {
            try
            {
                var client = new RestClient(Environment.GetEnvironmentVariable("MST_BASE_URL") + pathUrlCrudManytomany + "/" + payload.EmpSID);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.DELETE);
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                if (response.Server == null)
                {
                    return new Result<EmployeeModel>()
                    {
                        Succeeded = false,
                        Message = GlobalVariable.serviceNotAvailabel
                    };
                }
                var deserializeResponse = JsonConvert.DeserializeObject<Result<EmployeeModel>>(response.Content);

                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<EmployeeModel>()
                {
                    Succeeded = false,
                    Message = ex.Message
                };
            }
        }
        #endregion
    }
}
