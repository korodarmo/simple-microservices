﻿using System;
namespace ePM_frontend.Services.System.Models
{
    public class MenuModel
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public int RecordFlag { get; set; }
        //------------------------------
        public string MenuID { get; set; }
        public string MenuName { get; set; }
        public string MenuDescription { get; set; }
        public string MenuIcon { get; set; }
        public int Status { get; set; }
        public int Ordering { get; set; }
    }
}