﻿using System;

namespace ePM_frontend.Services.System.Models
{
    public class RoleModel
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public int RecordFlag { get; set; }
        public string RoleID { get; set; }
        public string RoleName { get; set; }
        public string RoleAccess { get; set; }
    }
}