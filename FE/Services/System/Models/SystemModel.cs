﻿using System;
namespace ePM_frontend.Services.System.Models
{
    public class SystemModel
    {
        public string IdAkun { get; set; }
        public string Token { get; set; }
        public DateTime ExpiredTime { get; set; }
    }
}