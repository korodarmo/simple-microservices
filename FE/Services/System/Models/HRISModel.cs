﻿using System;

namespace ePM_frontend.Services.Master.Models
{
    public class HRISModel
    {

        public string Cluster { get; set; }
        public string CompCode { get; set; }
        public string EmpNIK { get; set; }
        public string EmpName { get; set; }
        public string JobTtlCode { get; set; }
        public string JobTtlName { get; set; }
        public string OrgCode { get; set; }
        public string OrgName { get; set; }
        public string EmailAddr { get; set; }
        public string ProgCode { get; set; }
        public string ProgName { get; set; }
        public string ClusterCode { get; set; }
        public string ClusterName { get; set; }
        public string ScaleCode { get; set; }
        public string ScaleName { get; set; }
        public string SuperiorNIK { get; set; }
        public string SuperiorName { get; set; }
    }
}
