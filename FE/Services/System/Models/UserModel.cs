﻿using System;

namespace ePM_frontend.Services.System.Models
{
    public class UserModel
    {
        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public int RecordFlag { get; set; }
        public string UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Rolename { get; set; }
        public string RoleAccess { get; set; }
        public string Instance { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public int isPlanner { get; set; }
        public int isSupervisor { get; set; }
        public string line { get; set; }
        public string competency { get; set; }
    }
}