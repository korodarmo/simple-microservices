﻿using System;

namespace ePM_frontend.Services.System.Models
{
    public class JobModel
    {

        public long RecordID { get; set; }
        public DateTime RecordTimestamp { get; set; }
        public int RecordStatus { get; set; }
        public int RecordFlag { get; set; }
        public string Job { get; set; }
        public string Role { get; set; }
    }
}