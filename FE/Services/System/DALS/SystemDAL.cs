﻿using Dapper;
using ePM_frontend.Helpers;
using ePM_frontend.Models;
using static ePM_frontend.Helpers.BaseHeader;
using ePM_frontend.Helpers.Dapper;
using ePM_frontend.Services.System.Interfaces;
using ePM_frontend.Services.System.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ePM_frontend.Services.Master.Models;

namespace ePM_frontend.Services.System.DALS
{
    public class SystemDAL : ApiHelper, ISystem
    {
        private readonly IDapper _dapper;
        private readonly IDapperHris _dapperHris;

        public SystemDAL(IDapper dapper, IDapperHris dapperHris) { _dapper = dapper; _dapperHris = dapperHris; }

        #region 'Role'
        public async Task<List<RoleModel>> getListRoles()
        {
            List<RoleModel> returnVal = new List<RoleModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_role.ToString()} where RecordStatus = 0";
                returnVal = await Task.FromResult(_dapper.GetAll<RoleModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<RoleModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<RoleModel>();
            }
            return returnVal;
        }

        public async Task<DataTableResultModel> getDataRoles(DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "RoleID";
                        break;
                    case 2:
                        orderBy = "RoleName";
                        break;
                    default:
                        break;
                }

                string sort = curOrder.dir?.ToUpper();
                int length = Args.length <= 0 ? 10 : Args.length;
                int page = Args.start;
                string keyword = Args.advSearch["keyword"];
                orderBy = string.IsNullOrEmpty(orderBy) ? "RoleID" : orderBy;
                var sql = $"SELECT * FROM {TableEnum.s_role.ToString()} where RecordStatus = 0 ";
                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(keyword))
                {
                    list_search_query.Add($" RoleID LIKE '%{keyword?.Trim()}%' ");
                    list_search_query.Add($" RoleName LIKE '%{keyword?.Trim()}%' ");
                }

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                int count = await Task.FromResult(_dapper.CountAll(sql));

                sql += $" ORDER BY {orderBy} {sort} OFFSET {page} ROWS FETCH NEXT {length} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<RoleModel>(sql));

                return new DataTableResultModel()
                {
                    data = items,
                    draw = Args.draw,
                    recordsTotal = count,
                    recordsFiltered = count
                };
            }
            catch (Exception ex)
            {
                return new DataTableResultModel()
                {
                    data = new List<RoleModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }
        public async Task<RoleModel> getRoleByRoleID(string RoleID)
        {
            RoleModel returnVal = new RoleModel();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_role.ToString()} where RoleID = '" + RoleID + "'";
                returnVal = await Task.FromResult(_dapper.Get<RoleModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new RoleModel();
                }
            }
            catch (Exception)
            {
                returnVal = new RoleModel();
            }

            return returnVal;
        }
        public async Task<RoleModel> getRoleByID(long id)
        {
            RoleModel returnVal = new RoleModel();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_role.ToString()} where RecordID = " + id + "";
                returnVal = await Task.FromResult(_dapper.Get<RoleModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new RoleModel();
                }
            }
            catch (Exception)
            {
                returnVal = new RoleModel();
            }

            return returnVal;
        }
        public async Task<long> CreateRole(RoleModel payload)
        {
            long returnVal = 0;
            try
            {
                var sql = $"INSERT INTO {TableEnum.s_role.ToString()} " +
                         $"(RecordTimestamp,RoleID,RoleName,RoleAccess) " +
                         $"VALUES (@RecordTimestamp,@RoleID,@RoleName,@RoleAccess);" +
                         $" SELECT CAST(SCOPE_IDENTITY() as int);";
                var parameters = new DynamicParameters();
                parameters.Add("@RecordTimestamp", DateTime.Now);
                parameters.Add("@RoleID", payload.RoleID);
                parameters.Add("@RoleName", payload.RoleName);
                parameters.Add("@RoleAccess", payload.RoleAccess);
                returnVal = await Task.FromResult(_dapper.Insert(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        public async Task<RoleModel> UpdateRole(RoleModel payload)
        {
            RoleModel returnVal = new RoleModel();
            try
            {
                var sql = $"UPDATE {TableEnum.s_role.ToString()} " +
                    $" SET RoleAccess = @RoleAccess, Rolename = @Rolename where RecordID = @RecordID";

                var parameters = new DynamicParameters();
                parameters.Add("@RecordID", payload.RecordID);
                parameters.Add("@RoleAccess", payload.RoleAccess);
                parameters.Add("@RoleName", payload.RoleName);
                returnVal = await Task.FromResult(_dapper.Update<RoleModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        public async Task<RoleModel> DeleteRoleByID(long id)
        {
            RoleModel returnVal = new RoleModel();
            try
            {
                var sqlCheck = $"SELECT * FROM {TableEnum.s_role.ToString()} where RecordID = @id";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                returnVal = await Task.FromResult(_dapper.Get<RoleModel>(sqlCheck, parameters));
                if (returnVal == null)
                {
                    returnVal = new RoleModel();
                }
                else
                {
                    var sqlDelete = $"DELETE FROM {TableEnum.s_role.ToString()} where RecordID = @id";
                    await Task.FromResult(_dapper.Get<RoleModel>(sqlDelete, parameters));
                    return returnVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        #endregion

        #region 'user'
        public async Task<List<UserModel>> getDataPlanner()
        {
            List<UserModel> returnVal = new List<UserModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where Role = 'PLANNER' and isPlanner = 1 and RecordFlag = 0";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }

        public async Task<List<UserModel>> getUserByRole(string Role)
        {
            List<UserModel> returnVal = new List<UserModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where Role = '"+ Role + "' and RecordFlag = 0";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }

        public async Task<List<UserModel>> getDataAllSPVProduction()
        {
            List<UserModel> returnVal = new List<UserModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where Role = 'SPVPROD' and RecordFlag = 0";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }

        public async Task<List<UserModel>> getDataSPVProduction(string line)
        {
            List<UserModel> returnVal = new List<UserModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where Role = 'SPVPROD' and line = '"+line+"' and RecordFlag = 0";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }

        public async Task<List<UserModel>> getDataSPVTechnician(string filter)
        {
            List<UserModel> returnVal = new List<UserModel>();
            string filtering = string.IsNullOrEmpty(filter) ? " " : " and (UserID LIKE '%" + filter + "%' or Name LIKE '%" + filter + "%') ";

            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where Role = 'SPVENG' and RecordFlag = 0 {filtering}";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }
        public async Task<List<UserModel>> getDataTechnician(string filter)
        {
            List<UserModel> returnVal = new List<UserModel>();
            string filtering = string.IsNullOrEmpty(filter) ? " " : " and (UserID LIKE '%" + filter + "%' or Name LIKE '%" + filter + "%') ";
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where Role = 'TECHNICIAN' and RecordFlag = 0 {filtering}";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }

        public async Task<Result<List<HRISModel>>> getDataTechnicianHris(string filter)
        {

            try
            {
                var url = Environment.GetEnvironmentVariable("MST_BASE_URL") + Environment.GetEnvironmentVariable("MST_MC_MASTER_COMPETENCY") + "/getTechnician?filter=" + filter;
                var client = new RestClient(url);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("MST_APP_KEY"));
                var request = new RestRequest(Method.GET);
                request = requestAddStandardHeader(request);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<Result<List<HRISModel>>>(response.Content);
                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<List<HRISModel>>()
                {
                    Message = ex.Message
                };
            }
        }

        public async Task<DataTableResultModel> getDataUser(DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "Username";
                        break;
                    case 2:
                        orderBy = "Name";
                        break;
                    case 3:
                        orderBy = "Instance";
                        break;
                    case 4:
                        orderBy = "RoleName";
                        break;
                    default:
                        break;
                }

                string sort = curOrder.dir?.ToUpper();
                int length = Args.length <= 0 ? 10 : Args.length;
                int page = Args.start;
                string keyword = Args.advSearch["keyword"];
                orderBy = string.IsNullOrEmpty(orderBy) ? "UserID" : orderBy;
                var sql = $"SELECT u.*, r.RoleName FROM {TableEnum.s_user.ToString()} u " +
                    $"LEFT JOIN {TableEnum.s_role.ToString()} r ON r.RoleID = u.Role where u.RecordStatus = 0  ";
                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(keyword))
                {
                    list_search_query.Add($" Username LIKE '%{keyword?.Trim()}%' ");
                    list_search_query.Add($" Name LIKE '%{keyword?.Trim()}%' ");
                    list_search_query.Add($" Instance LIKE '%{keyword?.Trim()}%' ");
                    list_search_query.Add($" RoleName LIKE '%{keyword?.Trim()}%' ");
                }

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                int count = await Task.FromResult(_dapper.CountAll(sql));

                sql += $" ORDER BY {orderBy} {sort} OFFSET {page} ROWS FETCH NEXT {length} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<UserModel>(sql));

                return new DataTableResultModel()
                {
                    data = items,
                    draw = Args.draw,
                    recordsTotal = count,
                    recordsFiltered = count
                };
            }
            catch (Exception ex)
            {
                return new DataTableResultModel()
                {
                    data = new List<UserModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }
        public async Task<List<UserModel>> getListUserByRole(IDictionary<string, dynamic> payload)
        {
            List<UserModel> returnVal = new List<UserModel>();
            try
            {
                var role = payload["role"];//OPERATOR/SPVPROD
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where role = '" + role + "'";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return returnVal;
        }

        public async Task<HRISModel> checkuserNameFromHris(string username)
        {
            HRISModel returnVal = new HRISModel();
            try
            {
                var sql = $"SELECT TOP 1 *  FROM {TableEnum.v_PAComp.ToString()} where LOWER(EmailAddr) = '{username}'";
                var parameters = new DynamicParameters();
                returnVal = await Task.FromResult(_dapperHris.Get<HRISModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new HRISModel();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                returnVal = new HRISModel();
            }

            return returnVal;
        }

        public async Task<UserModel> GetUserDataByUsername(string username)
        {
            UserModel returnVal = new UserModel();
            try
            {
                var sql = $"SELECT u.*,r.RoleName,r.RoleAccess FROM {TableEnum.s_user.ToString()} u " +
                    $" LEFT JOIN {TableEnum.s_role.ToString()} r ON r.RoleID = u.Role where u.Username = '{username}'";
                var parameters = new DynamicParameters();
                returnVal = await Task.FromResult(_dapper.Get<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new UserModel();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                returnVal = new UserModel();
            }

            return returnVal;
        }
        public async Task<UserModel> getUserByUserID(string UserID)
        {
            UserModel returnVal = new UserModel();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where UserID = '" + UserID + "'";
                returnVal = await Task.FromResult(_dapper.Get<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new UserModel();
                }
            }
            catch (Exception)
            {
                returnVal = new UserModel();
            }

            return returnVal;
        }
        public async Task<UserModel> getUserByID(long id)
        {
            UserModel returnVal = new UserModel();
            try
            {
                var sql = $"SELECT u.*,r.RoleName,r.RoleAccess FROM {TableEnum.s_user.ToString()} u " +
                    $" LEFT JOIN {TableEnum.s_role.ToString()} r ON r.RoleID = u.Role where u.RecordID =" + id;
                returnVal = await Task.FromResult(_dapper.Get<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new UserModel();
                }
            }
            catch (Exception)
            {
                returnVal = new UserModel();
            }

            return returnVal;
        }
        public async Task<long> CreateUser(UserModel payload)
        {
            long returnVal = 0;
            try
            {
                var sql = $"INSERT INTO {TableEnum.s_user.ToString()} " +
                         $"(RecordTimestamp,UserID,Username,Password,Name,Role,Instance,OrganizationID,OrganizationName,isPlanner,isSupervisor,line,competency) " +
                         $"VALUES (@RecordTimestamp,@UserID,@Username,@Password,@Name,@Role,@Instance,@OrganizationID,@OrganizationName,@isPlanner,@isSupervisor,@line,@competency);" +
                         $" SELECT CAST(SCOPE_IDENTITY() as int);";
                var parameters = new DynamicParameters();
                parameters.Add("@RecordTimestamp", DateTime.Now);
                parameters.Add("@UserID", payload.UserID);
                parameters.Add("@Username", payload.Username);
                parameters.Add("@Password", payload.Password);
                parameters.Add("@Name", payload.Name);
                parameters.Add("@Role", payload.Role);
                parameters.Add("@Instance", payload.Instance);
                parameters.Add("@OrganizationID", payload.OrganizationID);
                parameters.Add("@OrganizationName", payload.OrganizationName);
                parameters.Add("@isPlanner", payload.isPlanner);
                parameters.Add("@isSupervisor", payload.isSupervisor);
                parameters.Add("@line", payload.line);
                parameters.Add("@competency", payload.competency);
                returnVal = await Task.FromResult(_dapper.Insert(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        public async Task<UserModel> UpdateUser(UserModel payload)
        {
            UserModel returnVal = new UserModel();
            try
            {
                var sql = $"UPDATE {TableEnum.s_user.ToString()} " +
                    $" SET Name = @Name, Role = @Role, Instance = @Instance, OrganizationID = @OrganizationID, OrganizationName = @OrganizationName, " +
                    $"isPlanner = @isPlanner, isSupervisor = @isSupervisor , line = @line , competency = @competency where RecordID = @RecordID";

                var parameters = new DynamicParameters();
                parameters.Add("@RecordID", payload.RecordID);
                parameters.Add("@Username", payload.Username);
                parameters.Add("@Name", payload.Name);
                parameters.Add("@Role", payload.Role);
                parameters.Add("@Instance", payload.Instance);
                parameters.Add("@OrganizationID", payload.OrganizationID);
                parameters.Add("@OrganizationName", payload.OrganizationName);
                parameters.Add("@isPlanner", payload.isPlanner);
                parameters.Add("@isSupervisor", payload.isSupervisor);
                parameters.Add("@line", payload.line);
                parameters.Add("@competency", payload.competency);
                returnVal = await Task.FromResult(_dapper.Update<UserModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        public async Task<UserModel> DeleteUserByID(long id)
        {
            UserModel returnVal = new UserModel();
            try
            {
                var sqlCheck = $"SELECT * FROM {TableEnum.s_user.ToString()} where RecordID = @id";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                returnVal = await Task.FromResult(_dapper.Get<UserModel>(sqlCheck, parameters));
                if (returnVal == null)
                {
                    returnVal = new UserModel();
                }
                else
                {
                    var sqlDelete = $"DELETE FROM {TableEnum.s_user.ToString()} where RecordID = @id";
                    await Task.FromResult(_dapper.Get<UserModel>(sqlDelete, parameters));
                    return returnVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        

        public async Task<JobModel> getRoleFromMapping(string job)
        {
            JobModel returnVal = new JobModel();
            try
            {
                var sql = $"select * FROM {TableEnum.s_job.ToString()} WHERE Job = '" + job + "'";
                returnVal = await Task.FromResult(_dapper.Get<JobModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new JobModel();
                }
            }
            catch (Exception)
            {
                returnVal = new JobModel();
            }
            return returnVal;
        }

        public async Task<JobModel> getMappingJob(string role)
        {
            JobModel returnVal = new JobModel();
            try
            {
                var sql = $"select STUFF((SELECT ''',''' + Job FROM {TableEnum.s_job.ToString()} WHERE Role = '" + role + "' FOR XML PATH('')), 2, 2, '') AS Job";
                returnVal = await Task.FromResult(_dapper.Get<JobModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new JobModel();
                }
            }
            catch (Exception)
            {
                returnVal = new JobModel();
            }
            return returnVal;
        }

        public async Task<IDictionary<string, dynamic>> getMappingJobCompetencyByUserid(string userid)
        {
            IDictionary<string, dynamic> returnVal = new Dictionary<string, dynamic>();
            try
            { 
                // var sql = $"EXEC [fSYSGetCompetency] @pUserid=" + userid;
                var sql = $@" 
                    DECLARE @vGranulation int = 0;
                    DECLARE @vTabletting int = 0;
                    DECLARE @vBlistering int = 0;
                    DECLARE @vPacking int = 0; 
                    DECLARE @vCompetency nvarchar(max) = '';
                    declare @vVal int =0;declare  @vVal2  int =0;declare @vVal3  int =0;declare @vVal4  int =0;declare @vVal5  int =0;declare @vVal6 int =0;declare @vVal7 int =0;declare @vVal8 int =0;declare @vVal9 int =0;declare @vVal10 int = 0;  
                    declare @vVal11  int =0;declare @vVal12  int =0;declare @vVal13  int =0;declare @vVal14  int =0;declare @vVal15 int =0;declare @vVal16 int =0;declare @vVal17 int =0;declare @vVal18 int =0;declare @vVal19 int =0;declare @vVal20 int = 0;  
                    declare @vVal21  int =0;declare @vVal22  int =0;declare @vVal23  int =0;declare @vVal24  int =0;declare @vVal25 int =0;declare @vVal26 int =0;declare @vVal27 int =0;declare @vVal28 int =0;declare @vVal29 int =0;declare @vVal30 int = 0; 

                    IF EXISTS ( 
                        SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%DOKUMENTASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid )
                    begin 
                        set @vVal = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%LINE CLEARANCE%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal2 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%REKONSILIASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal3 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING PRODUK AFKIR%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal4 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%IPC%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal5 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%SAMPLING%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal6 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING MATERIAL DAN PRODUK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal7 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PEMBUATAN DAN PENGGUNAAN CAIRAN SANITASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal8 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MONITORING RH, SUHU DAN TEKANAN UDARA%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal9 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PENANGANAN LIMBAH%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal10 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%WERUM%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal11 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MESIN MIXING SOLID%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal12 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MESIN GRANULATOR%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal13 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MESIN FLUID BED DRYER%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal14 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MESIN AYAK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal15 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MESIN FINAL MIXING%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal16 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%TIMBANGAN LANTAI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal17 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%TIMBANGAN ANALITIK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal18 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%THERMOMETER%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal19 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%SERAH TERIMA MATERIAL DAN PRODUK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal20 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MOISTURE ANALYZER%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal21 = 1;
                    end
                    
                    if @vVal = 1 AND @vVal2 = 1 AND @vVal3 = 1 AND @vVal4 = 1 AND @vVal5 = 1 AND @vVal6 = 1 AND @vVal7 = 1 AND @vVal8 = 1 AND @vVal9 = 1
                        AND @vVal10 = 1 AND @vVal11 = 1 AND @vVal12 = 1 AND @vVal13 = 1 AND @vVal14 = 1 AND @vVal15 = 1 AND @vVal16 = 1 AND @vVal17 = 1 AND @vVal18 = 1 
                        AND @vVal19 = 1 AND @vVal20 = 1 AND @vVal21 = 1
                    begin
                        SET @vGranulation = 1;
                    end
                    
                    set @vVal = 0;set @vVal2 = 0;set @vVal3 = 0;set @vVal4 = 0;set @vVal5 = 0;set @vVal6 = 0;set @vVal7 = 0;set @vVal8 = 0;set @vVal9 = 0;set @vVal10 = 0; 
                    set @vVal11 = 0;set @vVal12 = 0;set @vVal13 = 0;set @vVal14 = 0;set @vVal15 = 0;set @vVal16 = 0;set @vVal17 = 0;set @vVal18 = 0;set @vVal19 = 0;set @vVal20 = 0; 
                    set @vVal21 = 0;set @vVal22 = 0;set @vVal23 = 0;set @vVal24 = 0;set @vVal25 = 0;set @vVal26 = 0;set @vVal27 = 0;set @vVal28 = 0;set @vVal29 = 0;set @vVal30 = 0; 

                    IF EXISTS ( 
                        SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%DOKUMENTASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid )
                    begin 
                        set @vVal = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%LINE CLEARANCE%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal2 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%REKONSILIASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal3 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING PRODUK AFKIR%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal4 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%IPC%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal5 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%SAMPLING%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal6 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING MATERIAL DAN PRODUK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal7 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PEMBUATAN DAN PENGGUNAAN CAIRAN SANITASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal8 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MONITORING RH, SUHU DAN TEKANAN UDARA%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal9 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PENANGANAN LIMBAH%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal10 = 1;
                    end  
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%TIMBANGAN LANTAI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal11 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%TIMBANGAN ANALITIK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal12 = 1;
                    end 
                    
                    if @vVal = 1 AND @vVal2 = 1 AND @vVal3 = 1 AND @vVal4 = 1 AND @vVal5 = 1 AND @vVal6 = 1 AND @vVal7 = 1 AND @vVal8 = 1 AND @vVal9 = 1
                        AND @vVal10 = 1 AND @vVal11 = 1 AND @vVal12 = 1
                    begin
                        SET @vTabletting = 1;
                    end
                    
                    set @vVal = 0;set @vVal2 = 0;set @vVal3 = 0;set @vVal4 = 0;set @vVal5 = 0;set @vVal6 = 0;set @vVal7 = 0;set @vVal8 = 0;set @vVal9 = 0;set @vVal10 = 0; 
                    set @vVal11 = 0;set @vVal12 = 0;set @vVal13 = 0;set @vVal14 = 0;set @vVal15 = 0;set @vVal16 = 0;set @vVal17 = 0;set @vVal18 = 0;set @vVal19 = 0;set @vVal20 = 0; 
                    set @vVal21 = 0;set @vVal22 = 0;set @vVal23 = 0;set @vVal24 = 0;set @vVal25 = 0;set @vVal26 = 0;set @vVal27 = 0;set @vVal28 = 0;set @vVal29 = 0;set @vVal30 = 0; 

                    
                    IF EXISTS ( 
                        SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%DOKUMENTASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid )
                    begin 
                        set @vVal = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%LINE CLEARANCE%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal2 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%REKONSILIASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal3 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING PRODUK AFKIR%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal4 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%IPC%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal5 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%SAMPLING%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal6 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING MATERIAL DAN PRODUK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal7 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PEMBUATAN DAN PENGGUNAAN CAIRAN SANITASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal8 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MONITORING RH, SUHU DAN TEKANAN UDARA%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal9 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PENANGANAN LIMBAH%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal10 = 1;
                    end  
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%TIMBANGAN LANTAI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal11 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%JANGKA SORONG%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal12 = 1;
                    end 
                    
                    if @vVal = 1 AND @vVal2 = 1 AND @vVal3 = 1 AND @vVal4 = 1 AND @vVal5 = 1 AND @vVal6 = 1 AND @vVal7 = 1 AND @vVal8 = 1 AND @vVal9 = 1
                        AND @vVal10 = 1 AND @vVal11 = 1 AND @vVal12 = 1
                    begin
                        SET @vBlistering = 1;
                    end
                
                    set @vVal = 0;set @vVal2 = 0;set @vVal3 = 0;set @vVal4 = 0;set @vVal5 = 0;set @vVal6 = 0;set @vVal7 = 0;set @vVal8 = 0;set @vVal9 = 0;set @vVal10 = 0; 
                    set @vVal11 = 0;set @vVal12 = 0;set @vVal13 = 0;set @vVal14 = 0;set @vVal15 = 0;set @vVal16 = 0;set @vVal17 = 0;set @vVal18 = 0;set @vVal19 = 0;set @vVal20 = 0; 
                    set @vVal21 = 0;set @vVal22 = 0;set @vVal23 = 0;set @vVal24 = 0;set @vVal25 = 0;set @vVal26 = 0;set @vVal27 = 0;set @vVal28 = 0;set @vVal29 = 0;set @vVal30 = 0; 
                    

                    IF EXISTS ( 
                        SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%DOKUMENTASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid )
                    begin 
                        set @vVal = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%LINE CLEARANCE%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal2 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%REKONSILIASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal3 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING PRODUK AFKIR%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal4 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%IPC%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal5 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%SAMPLING%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal6 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%HANDLING MATERIAL DAN PRODUK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal7 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PEMBUATAN DAN PENGGUNAAN CAIRAN SANITASI%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal8 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MONITORING RH, SUHU DAN TEKANAN UDARA%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal9 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%PENANGANAN LIMBAH%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal10 = 1;
                    end  
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%MESIN INKJET PRINTER%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal11 = 1;
                    end
                    IF EXISTS ( SELECT ClusterName from v_PAComp WHERE ClusterName LIKE '%SERAH TERIMA MATERIAL DAN PRODUK%' AND CAST(REPLACE(ISNULL(ScaleCode, '0'), 'Q', '') AS INT) >= 3 AND EmpNIK = @pUserid ) 
                    begin 
                        set @vVal12 = 1;
                    end 
                    
                    if @vVal = 1 AND @vVal2 = 1 AND @vVal3 = 1 AND @vVal4 = 1 AND @vVal5 = 1 AND @vVal6 = 1 AND @vVal7 = 1 AND @vVal8 = 1 AND @vVal9 = 1
                        AND @vVal10 = 1 AND @vVal11 = 1 AND @vVal12 = 1
                    begin
                        SET @vPacking = 1;
                    end
                
                    if @vGranulation = 1
                    BEGIN
                        if @vCompetency = ''
                        BEGIN
                            SET @vCompetency += 'GRANULATION';
                        END
                        ELSE
                        BEGIN
                            SET @vCompetency += ',GRANULATION';
                        END
                    END

                    
                    if @vTabletting = 1
                    BEGIN
                        if @vCompetency = ''
                        BEGIN
                            SET @vCompetency += 'TABLETTING';
                        END
                        ELSE
                        BEGIN
                            SET @vCompetency += ',TABLETTING';
                        END
                    END
                    
                    if @vBlistering = 1
                    BEGIN
                        if @vCompetency = ''
                        BEGIN
                            SET @vCompetency += 'BLISTERING';
                        END
                        ELSE
                        BEGIN
                            SET @vCompetency += ',BLISTERING';
                        END
                    END
                    
                    if @vPacking = 1
                    BEGIN
                        if @vCompetency = ''
                        BEGIN
                            SET @vCompetency += 'PACKING';
                        END
                        ELSE
                        BEGIN
                            SET @vCompetency += ',PACKING';
                        END
                    END

                    SELECT @vGranulation as granulation,
                        @vTabletting as tabletting,
                        @vBlistering as blistering,
                        @vPacking as packing,
                        @vCompetency as competency
                    ;
                ";
                var parameters = new DynamicParameters();
                parameters.Add("@pUserid", userid);
                Console.WriteLine(sql);
                Console.WriteLine("@userid:"+ userid);
                dynamic result = await Task.FromResult(_dapperHris.Get<dynamic>(sql, parameters));

                returnVal = ((IDictionary<string, dynamic>)result); 
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR GETTING COMPETENCY");
                Console.WriteLine(e.StackTrace);
            }
            return returnVal;
        }

        public async Task<List<UserModel>> getUserSupervisor()
        {
            List<UserModel> returnVal = new List<UserModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_user.ToString()} where isSupervisor = 1";
                returnVal = await Task.FromResult(_dapper.GetAll<UserModel>(sql));
                if (returnVal == null)
                {
                    returnVal = new List<UserModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<UserModel>();
            }
            return returnVal;
        }

        public async Task<Result<List<EbridgingIOModel>>> getErpInstance()
        {
            try
            {
                //var getConfig = ConfigLoader.getConfigMicroService("eBridging");
                var url = Environment.GetEnvironmentVariable("EBRIDGE_BASE_URL") + Environment.GetEnvironmentVariable("EBRIDGE_DATA_INSTANCE") + "?AppKey=" + Environment.GetEnvironmentVariable("EBRIDGE_APP_KEY");
                var client = new RestClient(url);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                var request = new RestRequest(Method.GET);
                request = requestAddStandardHeader(request);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<Result<List<EbridgingIOModel>>>(response.Content);
                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<List<EbridgingIOModel>>()
                {
                    Message = ex.Message
                };
            }
        }
        public async Task<Result<List<EbridgingIOModel>>> getErpOrganization()
        {
            try
            {
                //var getConfig = ConfigLoader.getConfigMicroService("eBridging");
                var url = Environment.GetEnvironmentVariable("EBRIDGE_BASE_URL") + Environment.GetEnvironmentVariable("EBRIDGE_DATA_ORGANIZATION") + "?AppKey=" + Environment.GetEnvironmentVariable("EBRIDGE_APP_KEY");
                var client = new RestClient(url);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                var request = new RestRequest(Method.GET);
                request = requestAddStandardHeader(request);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<Result<List<EbridgingIOModel>>>(response.Content);
                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return new Result<List<EbridgingIOModel>>()
                {
                    Message = ex.Message
                };
            }
        }
        #endregion

        public async Task<DataTableResultModel> getDataNotification(DataTableBindingModel Args,  BaseHeaderModel? baseHeader)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 0:
                        orderBy = "createdAt";
                        break;
                    case 1:
                        orderBy = "Title";
                        break;
                    case 2:
                        orderBy = "Message";
                        break;
                    case 3:
                        orderBy = "Sender";
                        break;
                    case 4:
                        orderBy = "isRead";
                        break;

                    default:
                        break;
                }

                //var getConfig = ConfigLoader.getConfigMicroService("Notifikasi");
                string reqUrl = Environment.GetEnvironmentVariable("NOTIF_BASE_URL") + Environment.GetEnvironmentVariable("NOTIF_MODULE") + "/getDatatables?AppKey=" + Environment.GetEnvironmentVariable("NOTIF_APP_KEY") +
                    $"&pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder.dir?.ToUpper()}";
                var client = new RestClient(reqUrl);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                var request = new RestRequest(Method.POST);
                Args.advSearch["Competency"] = baseHeader.Competency;
                Args.advSearch["RoleID"] = baseHeader.RoleID;
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<NotifikasiModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data,
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<NotifikasiModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }

        public async Task<DataTableResultModel> getServiceActivityNotification(DataTableBindingModel Args,  BaseHeaderModel? baseHeaderModel)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 0:
                        orderBy = "createdAt";
                        break;
                    case 1:
                        orderBy = "Title";
                        break;
                    case 2:
                        orderBy = "Message";
                        break;
                    case 3:
                        orderBy = "Sender";
                        break;
                    case 4:
                        orderBy = "isRead";
                        break;

                    default:
                        break;
                }

                //var getConfig = ConfigLoader.getConfigMicroService("Notifikasi");
                var reqUrl = Environment.GetEnvironmentVariable("ACT_BASE_URL") + Environment.GetEnvironmentVariable("ACT_MC_BATCHORDER") + "/notiflist" +

                    $"&pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder.dir?.ToUpper()}";
                var client = new RestClient(reqUrl);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("ACT_APP_KEY"));
                client.AddDefaultHeader("Header", JsonConvert.SerializeObject(baseHeaderModel));
                var request = new RestRequest(Method.POST);
                Args.advSearch["Competency"] = baseHeaderModel.Competency;
                Args.advSearch["RoleID"] = baseHeaderModel.RoleID;
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<NotifikasiModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data,
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<NotifikasiModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }

        public async Task<long> getActivityNotifUnRead(BaseHeaderModel? baseHeaderModel)
        {
            try
            { 
                var reqUrl = Environment.GetEnvironmentVariable("ACT_BASE_URL") + Environment.GetEnvironmentVariable("ACT_MC_BATCHORDER") + "/notifunread";
    
                var client = new RestClient(reqUrl);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("ACT_APP_KEY"));
                client.AddDefaultHeader("Header", JsonConvert.SerializeObject(baseHeaderModel));
                var request = new RestRequest(Method.POST); 
                IDictionary<string, dynamic> payload = new Dictionary<string, dynamic>();
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<ResponseModel>(response.Content);
                return (long)deserializeResponse.data;
            }
            catch (Exception e)
            {
                 Console.WriteLine(e.StackTrace);
            } 
            return 0;
            // return 15;
        }

        
        public async Task<List<NotifikasiModel>> getActivityLengOfNotif(BaseHeaderModel? baseHeaderModel)
        {
            List<NotifikasiModel> returnVal = new List<NotifikasiModel>();
            try
            { 
                var reqUrl = Environment.GetEnvironmentVariable("ACT_BASE_URL") + Environment.GetEnvironmentVariable("ACT_MC_BATCHORDER") + "/notiflist";
    
                var client = new RestClient(reqUrl);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("ACT_APP_KEY"));
                client.AddDefaultHeader("Header", JsonConvert.SerializeObject(baseHeaderModel));
                var request = new RestRequest(Method.POST); 
                IDictionary<string, dynamic> payload = new Dictionary<string, dynamic>();
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<NotifikasiModel>>(response.Content);
                returnVal = deserializeResponse.Data;

            }
            catch (Exception e)
            { 
                 Console.WriteLine(e.StackTrace);
            }
            return returnVal;
        }
        
        public async Task<DataTableResultModel> getDataAuditrail(BaseHeaderModel baseHeaderModel, DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 0:
                        orderBy = "createdAt";
                        break;
                    case 1:
                        orderBy = "UserID";
                        break;
                    case 2:
                        orderBy = "RoleID";
                        break;
                    case 3:
                        orderBy = "Method";
                        break;
                    case 4:
                        orderBy = "Description";
                        break;
                    case 5:
                        orderBy = "IPAddress";
                        break;

                    default:
                        break;
                }

                //var getConfig = ConfigLoader.getConfigMicroService("AuditTrails");
                var client = new RestClient(Environment.GetEnvironmentVariable("EAUD_BASE_URL") + Environment.GetEnvironmentVariable("EAUD_GET_DATATABLES") +
                    $"?pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder.dir?.ToUpper()}");
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                var request = new RestRequest(Method.POST);
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("EPM_KEY"));
                client.AddDefaultHeader("Header", JsonConvert.SerializeObject(baseHeaderModel));
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<PaginatedResult<AuditTrailsModel>>(response.Content);
                return new DataTableResultModel()
                {
                    data = deserializeResponse?.Data,
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.TotalCount) : 0,
                    error = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<AuditTrailsModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }

        public async Task<DataTableResultModel> getDataActivityLog(BaseHeaderModel baseHeaderModel, DataTableBindingModel Args)
        {
            try
            {
                var orderBy = "";
                var curOrder = Args.order?.FirstOrDefault();
                switch (curOrder?.column)
                {
                    case 1:
                        orderBy = "createdDate";
                        break;
                    case 2:
                        orderBy = "shiftName";
                        break;
                    case 3:
                        orderBy = "machineName";
                        break;
                    case 4:
                        orderBy = "operationName";
                        break;
                    case 5:
                        orderBy = "assignPersonel";
                        break;
                    case 6:
                        orderBy = "scheduledStart";
                        break;
                    case 7:
                        orderBy = "scheduledCompletion";
                        break;
                    case 8:
                        orderBy = "recordedTime";
                        break;
                    case 9:
                        orderBy = "activityName";
                        break;
                    case 10:
                        orderBy = "activityCode";
                        break;
                    case 11:
                        orderBy = "reason";
                        break;
                    case 12:
                        orderBy = "batchOrder";
                        break;
                    case 13:
                        orderBy = "batchNumber";
                        break;
                    case 14:
                        orderBy = "outputGood";
                        break;
                    case 15:
                        orderBy = "outputDefect";
                        break;
                    case 16:
                        orderBy = "status";
                        break;
                    default:
                        orderBy = "id";
                        break;
                }

                //var getConfig = ConfigLoader.getConfigMicroService("ServiceProduction");
                var reqUrl = Environment.GetEnvironmentVariable("ACT_BASE_URL") + Environment.GetEnvironmentVariable("ACT_MC_ACTIVITYLOG") + "/list" +
                    $"?pageNumber={Args.start}" +
                    $"&pageSize={Args.length}" +
                    $"&orderBy={orderBy}" +
                    $"&sort={curOrder.dir?.ToUpper()}";
                var client = new RestClient(reqUrl);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                var request = new RestRequest(Method.POST);
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("ACT_APP_KEY"));
                client.AddDefaultHeader("Header", JsonConvert.SerializeObject(baseHeaderModel));
                request = requestAddStandardHeaderWithBody(request, Args.advSearch);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<ResponseModel>(response.Content);

                return new DataTableResultModel()
                {
                    data = deserializeResponse.data,
                    draw = Args.draw,
                    recordsTotal = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.recordsTotal) : 0,
                    recordsFiltered = deserializeResponse != null ? Convert.ToInt32(deserializeResponse.recordsFiltered) : 0,
                    message = deserializeResponse?.Message
                };
            }
            catch (Exception ex)
            {

                return new DataTableResultModel()
                {
                    data = new List<AuditTrailsModel>(),
                    draw = Args.draw,
                    recordsTotal = 0,
                    recordsFiltered = 0,
                    error = ex.Message
                };
            }
        }

        public async Task<List<MenuModel>> getAllMenu()
        {
            List<MenuModel> returnVal = new List<MenuModel>();
            try
            {
                var sql = $"SELECT * FROM {TableEnum.s_menu.ToString()} where Status = 1  ORDER BY Status";
                returnVal = await Task.FromResult(_dapper.GetAll<MenuModel>(sql));

                if (returnVal == null)
                {
                    returnVal = new List<MenuModel>();
                }
            }
            catch (Exception)
            {
                returnVal = new List<MenuModel>();
            }

            return returnVal;
        }


        public async Task<dynamic> getDashboard(BaseHeaderModel baseHeaderModel, IDictionary<string, dynamic> payload)
        {
            try
            {
                //var getConfig = ConfigLoader.getConfigMicroService("ServiceProduction");
                var reqUrl = Environment.GetEnvironmentVariable("ACT_BASE_URL") + Environment.GetEnvironmentVariable("ACT_MC_ACTIVITYLOG") + "/dashboard/get";
                var client = new RestClient(reqUrl);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                var request = new RestRequest(Method.PATCH);
                client.AddDefaultHeader("ApiKey", Environment.GetEnvironmentVariable("ACT_APP_KEY"));
                client.AddDefaultHeader("Header", JsonConvert.SerializeObject(baseHeaderModel));
                request = requestAddStandardHeaderWithBody(request, payload);
                IRestResponse response = await client.ExecuteAsync(request);
                var deserializeResponse = JsonConvert.DeserializeObject<dynamic>(response.Content);
                return deserializeResponse;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


    }
}
