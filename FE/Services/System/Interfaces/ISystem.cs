﻿using ePM_frontend.Helpers;
using ePM_frontend.Models;
using ePM_frontend.Services.System.Models;
using static ePM_frontend.Helpers.BaseHeader;
using System.Collections.Generic;
using System.Threading.Tasks;
using ePM_frontend.Services.Master.Models;

namespace ePM_frontend.Services.System.Interfaces
{
    public interface ISystem
    {
        Task<DataTableResultModel> getDataNotification(DataTableBindingModel Args, BaseHeaderModel? baseHeader);
        Task<DataTableResultModel> getServiceActivityNotification(DataTableBindingModel Args, BaseHeaderModel? baseHeader);
        Task<long> getActivityNotifUnRead(BaseHeaderModel? baseHeader);
        Task<List<NotifikasiModel>> getActivityLengOfNotif(BaseHeaderModel? baseHeader);

        Task<List<RoleModel>> getListRoles();
        Task<DataTableResultModel> getDataRoles(DataTableBindingModel Args);
        Task<RoleModel> getRoleByID(long id);
        Task<RoleModel> getRoleByRoleID(string RoleID);
        Task<long> CreateRole(RoleModel payload);
        Task<RoleModel> UpdateRole(RoleModel payload);
        Task<RoleModel> DeleteRoleByID(long id);

        Task<DataTableResultModel> getDataUser(DataTableBindingModel Args);
        Task<List<UserModel>> getListUserByRole(IDictionary<string, dynamic> payload);
        Task<UserModel> getUserByID(long id);
        Task<UserModel> getUserByUserID(string UserID);
        Task<UserModel> GetUserDataByUsername(string username);
        Task<HRISModel> checkuserNameFromHris(string username); 
        Task<JobModel> getRoleFromMapping(string job); 
         Task<long> CreateUser(UserModel payload);
        Task<UserModel> UpdateUser(UserModel payload);
        Task<UserModel> DeleteUserByID(long id);
        Task<List<UserModel>> getUserByRole(string Role);
        Task<List<UserModel>> getDataTechnician(string term);
        Task<Result<List<HRISModel>>> getDataTechnicianHris(string term);
        Task<List<UserModel>> getDataSPVTechnician(string term);
        Task<List<UserModel>> getDataSPVProduction(string line);
        Task<List<UserModel>> getDataAllSPVProduction();
         Task<List<UserModel>> getDataPlanner();

        Task<Result<List<EbridgingIOModel>>> getErpInstance();
        Task<Result<List<EbridgingIOModel>>> getErpOrganization();
        Task<List<UserModel>> getUserSupervisor();

        Task<DataTableResultModel> getDataAuditrail(BaseHeaderModel baseHeaderModel, DataTableBindingModel Args);
        Task<DataTableResultModel> getDataActivityLog(BaseHeaderModel baseHeaderModel, DataTableBindingModel Args);
        Task<List<MenuModel>> getAllMenu();
        Task<dynamic> getDashboard(BaseHeaderModel baseHeaderModel, IDictionary<string, dynamic> payload);
        Task<JobModel> getMappingJob(string role);
        Task<IDictionary<string, dynamic>> getMappingJobCompetencyByUserid(string role);
        
    }
}
