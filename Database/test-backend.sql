/*
 Navicat Premium Data Transfer

 Source Server         : MSSQL - LOCAL
 Source Server Type    : SQL Server
 Source Server Version : 11002100
 Source Host           : DESKTOP-AM74PQ7:1433
 Source Catalog        : test-master
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 11002100
 File Encoding         : 65001

 Date: 05/06/2023 03:20:23
*/


-- ----------------------------
-- Table structure for Employee
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[Employee]') AND type IN ('U'))
	DROP TABLE [dbo].[Employee]
GO

CREATE TABLE [dbo].[Employee] (
  [EmployeeID] bigint  IDENTITY(1,1) NOT NULL,
  [RecordTime] datetime DEFAULT (getdate()) NOT NULL,
  [FirstName] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [LastName] varchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Email] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Phone] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[Employee] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of Employee
-- ----------------------------
SET IDENTITY_INSERT [dbo].[Employee] ON
GO

INSERT INTO [dbo].[Employee] ([EmployeeID], [RecordTime], [FirstName], [LastName], [Email], [Phone]) VALUES (N'8', N'2023-06-05 00:35:22.127', N'wahyu nugroho', N'indrawinata', N'korodarmo@gmail.com', N'0812142456960')
GO

SET IDENTITY_INSERT [dbo].[Employee] OFF
GO


-- ----------------------------
-- Table structure for EmployeeAddress
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeAddress]') AND type IN ('U'))
	DROP TABLE [dbo].[EmployeeAddress]
GO

CREATE TABLE [dbo].[EmployeeAddress] (
  [AddressID] bigint  IDENTITY(1,1) NOT NULL,
  [EmployeeID] bigint  NOT NULL,
  [Type] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Address] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[EmployeeAddress] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of EmployeeAddress
-- ----------------------------
SET IDENTITY_INSERT [dbo].[EmployeeAddress] ON
GO

INSERT INTO [dbo].[EmployeeAddress] ([AddressID], [EmployeeID], [Type], [Address]) VALUES (N'1', N'8', N'PRIBADI', N'adawaw')
GO

INSERT INTO [dbo].[EmployeeAddress] ([AddressID], [EmployeeID], [Type], [Address]) VALUES (N'3', N'8', N'ORANGTUA', N'adad')
GO

SET IDENTITY_INSERT [dbo].[EmployeeAddress] OFF
GO


-- ----------------------------
-- Table structure for EmployeeDetails
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeDetails]') AND type IN ('U'))
	DROP TABLE [dbo].[EmployeeDetails]
GO

CREATE TABLE [dbo].[EmployeeDetails] (
  [RecordID] bigint  IDENTITY(1,1) NOT NULL,
  [EmployeeID] bigint  NOT NULL,
  [PassportNumber] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[EmployeeDetails] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of EmployeeDetails
-- ----------------------------
SET IDENTITY_INSERT [dbo].[EmployeeDetails] ON
GO

INSERT INTO [dbo].[EmployeeDetails] ([RecordID], [EmployeeID], [PassportNumber]) VALUES (N'3', N'8', N'1234568')
GO

SET IDENTITY_INSERT [dbo].[EmployeeDetails] OFF
GO


-- ----------------------------
-- Table structure for EmployeeSkill
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeeSkill]') AND type IN ('U'))
	DROP TABLE [dbo].[EmployeeSkill]
GO

CREATE TABLE [dbo].[EmployeeSkill] (
  [EmpSID] bigint  IDENTITY(1,1) NOT NULL,
  [EmployeeID] bigint  NOT NULL,
  [SkillID] bigint  NULL
)
GO

ALTER TABLE [dbo].[EmployeeSkill] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of EmployeeSkill
-- ----------------------------
SET IDENTITY_INSERT [dbo].[EmployeeSkill] ON
GO

INSERT INTO [dbo].[EmployeeSkill] ([EmpSID], [EmployeeID], [SkillID]) VALUES (N'3', N'8', N'4')
GO

INSERT INTO [dbo].[EmployeeSkill] ([EmpSID], [EmployeeID], [SkillID]) VALUES (N'4', N'8', N'6')
GO

INSERT INTO [dbo].[EmployeeSkill] ([EmpSID], [EmployeeID], [SkillID]) VALUES (N'6', N'8', N'3')
GO

SET IDENTITY_INSERT [dbo].[EmployeeSkill] OFF
GO


-- ----------------------------
-- Table structure for SkillDetails
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[SkillDetails]') AND type IN ('U'))
	DROP TABLE [dbo].[SkillDetails]
GO

CREATE TABLE [dbo].[SkillDetails] (
  [SkillID] bigint  IDENTITY(1,1) NOT NULL,
  [Description] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[SkillDetails] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of SkillDetails
-- ----------------------------
SET IDENTITY_INSERT [dbo].[SkillDetails] ON
GO

INSERT INTO [dbo].[SkillDetails] ([SkillID], [Description]) VALUES (N'3', N'PHP')
GO

INSERT INTO [dbo].[SkillDetails] ([SkillID], [Description]) VALUES (N'4', N'C#')
GO

INSERT INTO [dbo].[SkillDetails] ([SkillID], [Description]) VALUES (N'5', N'JAVA')
GO

INSERT INTO [dbo].[SkillDetails] ([SkillID], [Description]) VALUES (N'6', N'RUBY')
GO

SET IDENTITY_INSERT [dbo].[SkillDetails] OFF
GO


-- ----------------------------
-- Primary Key structure for table Employee
-- ----------------------------
ALTER TABLE [dbo].[Employee] ADD CONSTRAINT [PK__Employee__7AD04FF14A94E3AE] PRIMARY KEY CLUSTERED ([EmployeeID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EmployeeAddress
-- ----------------------------
ALTER TABLE [dbo].[EmployeeAddress] ADD CONSTRAINT [PK__Employee__7AD04FF1F25BD568_copy1] PRIMARY KEY CLUSTERED ([AddressID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EmployeeDetails
-- ----------------------------
ALTER TABLE [dbo].[EmployeeDetails] ADD CONSTRAINT [PK__Employee__FBDF78C9FA6EDF30] PRIMARY KEY CLUSTERED ([RecordID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table EmployeeSkill
-- ----------------------------
ALTER TABLE [dbo].[EmployeeSkill] ADD CONSTRAINT [PK__Employee__D4C8A911BA0CF60E] PRIMARY KEY CLUSTERED ([EmpSID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table SkillDetails
-- ----------------------------
ALTER TABLE [dbo].[SkillDetails] ADD CONSTRAINT [PK__Employee__7AD04FF1F25BD568_copy2_copy1] PRIMARY KEY CLUSTERED ([SkillID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table EmployeeAddress
-- ----------------------------
ALTER TABLE [dbo].[EmployeeAddress] ADD CONSTRAINT [FK__EmployeeA__Emplo__5FB337D6] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID]) ON DELETE CASCADE ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table EmployeeDetails
-- ----------------------------
ALTER TABLE [dbo].[EmployeeDetails] ADD CONSTRAINT [FK__EmployeeD__Emplo__6383C8BA] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID]) ON DELETE CASCADE ON UPDATE NO ACTION
GO


-- ----------------------------
-- Foreign Keys structure for table EmployeeSkill
-- ----------------------------
ALTER TABLE [dbo].[EmployeeSkill] ADD CONSTRAINT [FK__EmployeeS__Emplo__6754599E] FOREIGN KEY ([EmployeeID]) REFERENCES [dbo].[Employee] ([EmployeeID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[EmployeeSkill] ADD CONSTRAINT [FK__EmployeeS__Skill__68487DD7] FOREIGN KEY ([SkillID]) REFERENCES [dbo].[SkillDetails] ([SkillID]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

