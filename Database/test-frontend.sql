/*
 Navicat Premium Data Transfer

 Source Server         : MSSQL - LOCAL
 Source Server Type    : SQL Server
 Source Server Version : 11002100
 Source Host           : DESKTOP-AM74PQ7:1433
 Source Catalog        : test-frontend
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 11002100
 File Encoding         : 65001

 Date: 05/06/2023 03:20:48
*/


-- ----------------------------
-- Table structure for s_job
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[s_job]') AND type IN ('U'))
	DROP TABLE [dbo].[s_job]
GO

CREATE TABLE [dbo].[s_job] (
  [RecordID] bigint  IDENTITY(1,1) NOT NULL,
  [RecordTimestamp] datetime  NOT NULL,
  [RecordStatus] int  NOT NULL,
  [RecordFlag] int  NOT NULL,
  [Job] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Role] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[s_job] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Table structure for s_menu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[s_menu]') AND type IN ('U'))
	DROP TABLE [dbo].[s_menu]
GO

CREATE TABLE [dbo].[s_menu] (
  [RecordID] bigint  IDENTITY(1,1) NOT NULL,
  [RecordTimestamp] datetime  NOT NULL,
  [RecordStatus] int  NOT NULL,
  [RecordFlag] int  NOT NULL,
  [MenuID] nvarchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MenuName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MenuDescription] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [MenuIcon] nvarchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Status] int  NOT NULL,
  [Ordering] int  NOT NULL
)
GO

ALTER TABLE [dbo].[s_menu] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of s_menu
-- ----------------------------
SET IDENTITY_INSERT [dbo].[s_menu] ON
GO

INSERT INTO [dbo].[s_menu] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [MenuID], [MenuName], [MenuDescription], [MenuIcon], [Status], [Ordering]) VALUES (N'1', N'2018-02-19 18:09:31.000', N'0', N'1', N'M1', N'Home', N'Dashboard', N'grid-outline', N'1', N'1')
GO

INSERT INTO [dbo].[s_menu] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [MenuID], [MenuName], [MenuDescription], [MenuIcon], [Status], [Ordering]) VALUES (N'11', N'2018-02-19 18:09:31.000', N'0', N'1', N'N11', N'Query', N'Relational Database', N'checkmark-square-outline', N'1', N'11')
GO

INSERT INTO [dbo].[s_menu] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [MenuID], [MenuName], [MenuDescription], [MenuIcon], [Status], [Ordering]) VALUES (N'22', N'2018-02-19 18:09:31.000', N'0', N'0', N'N11P1', N'CrudBasic', N'CRUD BASIC', N'', N'1', N'1')
GO

INSERT INTO [dbo].[s_menu] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [MenuID], [MenuName], [MenuDescription], [MenuIcon], [Status], [Ordering]) VALUES (N'23', N'2018-02-19 18:09:31.000', N'0', N'0', N'N11P2', N'Onetoone', N'SQL 1-1', N'', N'1', N'2')
GO

INSERT INTO [dbo].[s_menu] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [MenuID], [MenuName], [MenuDescription], [MenuIcon], [Status], [Ordering]) VALUES (N'24', N'2018-02-19 18:09:31.000', N'0', N'0', N'N11P3', N'Onetomany', N'SQL 1-N', N'', N'1', N'3')
GO

INSERT INTO [dbo].[s_menu] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [MenuID], [MenuName], [MenuDescription], [MenuIcon], [Status], [Ordering]) VALUES (N'25', N'2018-02-19 18:09:31.000', N'0', N'0', N'N11P4', N'Manytomany', N'SQL N-N', N'', N'1', N'3')
GO

SET IDENTITY_INSERT [dbo].[s_menu] OFF
GO


-- ----------------------------
-- Table structure for s_role
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[s_role]') AND type IN ('U'))
	DROP TABLE [dbo].[s_role]
GO

CREATE TABLE [dbo].[s_role] (
  [RecordID] bigint  IDENTITY(1,1) NOT NULL,
  [RecordTimestamp] datetime  NULL,
  [RecordStatus] int DEFAULT ((0)) NULL,
  [RecordFlag] int DEFAULT ((0)) NOT NULL,
  [RoleID] nvarchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [RoleName] nvarchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [RoleAccess] nvarchar(max) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[s_role] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of s_role
-- ----------------------------
SET IDENTITY_INSERT [dbo].[s_role] ON
GO

INSERT INTO [dbo].[s_role] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [RoleID], [RoleName], [RoleAccess]) VALUES (N'1', N'2021-12-12 06:46:44.547', N'0', N'1', N'_SU', N'SUPERUSER', N'M1=11111000|M10=11111000|M2=11111000|M3=11111000|M4=10000000|M5=10000000|M6=11111000|M7=11111000|M8=11111000|M9=11111000|N11=11111000|N11P1=11111000|N11P2=11111000|N11P3=11111000|N11P4=11111000|N11P5=11111000|N11P6=11111000|N11P7=11111000|N12=11111000|N12P1=11111000|N12P2=11111000|N12P3=11111000|N12P4=11111000|N12P5=11111000|N12P6=11111000|N12P7=11111000|N12P8=11111000|N12P9=11111000|N13=11111000|N13P1=11111000|N13P2=11111000|')
GO

SET IDENTITY_INSERT [dbo].[s_role] OFF
GO


-- ----------------------------
-- Table structure for s_user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[s_user]') AND type IN ('U'))
	DROP TABLE [dbo].[s_user]
GO

CREATE TABLE [dbo].[s_user] (
  [RecordID] bigint  IDENTITY(1,1) NOT NULL,
  [RecordTimestamp] datetime  NULL,
  [RecordStatus] int DEFAULT ((0)) NULL,
  [RecordFlag] int DEFAULT ((0)) NOT NULL,
  [UserID] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Username] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [Password] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Name] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Role] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [Instance] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT '' NULL,
  [OrganizationID] int DEFAULT ((0)) NULL,
  [OrganizationName] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT '' NULL,
  [isPlanner] int DEFAULT ((0)) NULL,
  [isSupervisor] int DEFAULT ((0)) NULL,
  [line] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [competency] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS DEFAULT '' NULL
)
GO

ALTER TABLE [dbo].[s_user] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of s_user
-- ----------------------------
SET IDENTITY_INSERT [dbo].[s_user] ON
GO

INSERT INTO [dbo].[s_user] ([RecordID], [RecordTimestamp], [RecordStatus], [RecordFlag], [UserID], [Username], [Password], [Name], [Role], [Instance], [OrganizationID], [OrganizationName], [isPlanner], [isSupervisor], [line], [competency]) VALUES (N'1', N'2022-03-16 18:15:21.900', N'1', N'0', N'superuser', N'superuser', N'0BAEA2F0AE20150DB78F58CDDAC442A9', N'SUPERUSER', N'_SU', N'', N'0', N'', N'0', N'0', NULL, N'')
GO

SET IDENTITY_INSERT [dbo].[s_user] OFF
GO


-- ----------------------------
-- Primary Key structure for table s_job
-- ----------------------------
ALTER TABLE [dbo].[s_job] ADD CONSTRAINT [PK__t9040__B222597ED7D80CB9_copy1] PRIMARY KEY CLUSTERED ([Job])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table s_menu
-- ----------------------------
ALTER TABLE [dbo].[s_menu] ADD CONSTRAINT [PK__t9040__B222597ED7D80CB9] PRIMARY KEY CLUSTERED ([MenuID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table s_role
-- ----------------------------
ALTER TABLE [dbo].[s_role] ADD CONSTRAINT [PK__t9020__3D3B780D34D4FAF9] PRIMARY KEY CLUSTERED ([RoleID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table s_user
-- ----------------------------
ALTER TABLE [dbo].[s_user] ADD CONSTRAINT [PK__t9010__985AF8C0DE760307] PRIMARY KEY CLUSTERED ([UserID])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

