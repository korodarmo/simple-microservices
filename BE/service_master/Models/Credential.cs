﻿using LiteDB;
using System;

namespace service_master.Models
{
    public class Credential
    {
        [BsonId]
        public int id { get; set; }
        public string IdAkun { get; set; } = "1";
        public string Token { get; set; }
        public DateTime expiredTime { get; set; }
        public string CLUSTERCODE { get; set; }
        public string CN { get; set; }
        public string COMPANY { get; set; }
        public string DISPLAYNAME { get; set; }
        public string DN { get; set; }
        public string EMAIL { get; set; }
        public string EmployeeNumber { get; set; }
        public string FIRST_NAME { get; set; }
        public string GROUP { get; set; }
        public string GROUPADMIN { get; set; }
        public string LAST_NAME { get; set; }
        public string LOGON_NAME { get; set; }
        public string UPN { get; set; }
    }
}