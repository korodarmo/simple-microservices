﻿namespace service_master.Models
{
    public class AuditTrailsModel
    {
        public string AppKey { get; set; }
        public string UserID { get; set; }
        public string RoleID { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
        public string Parameter { get; set; }
        public string ValueOld { get; set; }
        public string ValueNew { get; set; }
        public string IPAddress { get; set; }
        public string Source { get; set; }
    }
}
