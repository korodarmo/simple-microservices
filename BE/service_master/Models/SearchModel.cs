﻿using System;
using System.Text.Json.Serialization;

namespace service_master.Models
{
    public class SearchDefaultModel
    {
        public string keyword { get; set; }
        public string name { get; set; }
        
    }

    public class SearchConfigurationActivityModel
    {
        [JsonIgnore]
        public string machineName { get; set; }
        public long operationId { get; set; }
        public long activityId { get; set; }
        public string tklCode { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
    }
}
