﻿using Dapper;
using service_master.Helpers;
using service_master.Helpers.SqlServer.Dapper;
using service_master.Models;
using service_master.Services.Master.Interfaces;
using service_master.Services.Master.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace service_master.Services.Master.DALS
{
    public class EmployeeDAL : IEmployee
    {
        private readonly IDapperMain _dapper;
        public EmployeeDAL(IDapperMain dapper) { _dapper = dapper; }

        #region 'One To One'
        public async Task<DTPagination<EmployeeModel>> GetOnetoOne(SearchDefaultModel search, string orderBy = "FirstName", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC)
        {
            try
            {
                page_size = page_size <= 0 ? 10 : page_size;
                orderBy = string.IsNullOrEmpty(orderBy) ? "RecordTime" : orderBy;

                var sql = @$"SELECT emp.* , emp_d.PassportNumber as PassportNumber
                                FROM {TableEnum.Employee} emp
                                LEFT JOIN {TableEnum.EmployeeDetails} emp_d on emp.EmployeeID = emp_d.EmployeeID
                          where 1=1
                ";

                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(search.keyword))
                {
                    list_search_query.Add($" emp.FirstName LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp.LastName LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp.Email LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp.Phone LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp_d.PassportNumber LIKE '%{search.keyword?.Trim()}%' ");
                }

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                long count = await Task.FromResult(_dapper.CountAll(sql));

                sql += $" ORDER BY {orderBy} {sort.ToString()} OFFSET {page_number} ROWS FETCH NEXT {page_size} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<EmployeeModel>(sql));

                return DTPagination<EmployeeModel>.Success(items, count, page_number, page_size);
            }
            catch (Exception ex)
            {
                return DTPagination<EmployeeModel>.Failure(new List<string>() { ex.Message });
            }
        }

        public async Task<List<EmployeeModel>> getListEmployee(string filter)
        {
            List<EmployeeModel> returnVal = new List<EmployeeModel>();
            string filtering = string.IsNullOrEmpty(filter) ? " " : " and (FirstName LIKE '%" + filter + "%' OR LastName LIKE '%" + filter + "%' OR Email LIKE '%" + filter + "%' )";
            try
            {
                var sql = $@" SELECT * FROM {TableEnum.Employee.ToString()}  where EmployeeID != 0 {filtering}";
                var parameters = new DynamicParameters();
                returnVal = await Task.FromResult(_dapper.GetAll<EmployeeModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnVal;
        }

        public async Task<EmployeeModel> getOnetoOneByID(string id)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = @$"SELECT emp.*
                                , emp_d.PassportNumber as PassportNumber
                                FROM {TableEnum.Employee} emp
                                LEFT JOIN {TableEnum.EmployeeDetails} emp_d on emp.EmployeeID = emp_d.EmployeeID
                          where emp.EmployeeID = @id";
                var parameters = new DynamicParameters();
                parameters.Add("@id", id);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
            }
            catch (Exception)
            {
                returnVal = new EmployeeModel();
            }

            return returnVal;
        }
        public async Task<EmployeeModel> getCheckData(string FirstName, string LastName, string PassportNumber)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = @$"SELECT emp.*
                                , emp_d.PassportNumber as PassportNumber
                                FROM {TableEnum.Employee} emp
                                LEFT JOIN {TableEnum.EmployeeDetails} emp_d on emp.EmployeeID = emp_d.EmployeeID
                          where (emp.FirstName = @FirstName AND emp.LastName = @LastName)  OR emp_d.PassportNumber = @PassportNumber";
                var parameters = new DynamicParameters();
                parameters.Add("@FirstName", FirstName);
                parameters.Add("@LastName", LastName);
                parameters.Add("@PassportNumber", PassportNumber);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
            }
            catch (Exception)
            {
                returnVal = new EmployeeModel();
            }

            return returnVal;
        }

        public async Task<long> CreateOnetoOne(EmployeeModel payload)
        {
            long returnValEmployee = 0;
            long returnVal = 0;
            try
            {
                var employee = $"INSERT INTO {TableEnum.Employee.ToString()} " +
                   $"(RecordTime, FirstName, LastName, Email ,Phone) " +
                   $"VALUES (@RecordTime, @FirstName, @LastName, @Email ,@Phone);" +
                   $" SELECT CAST(SCOPE_IDENTITY() as int);";

                var parameters = new DynamicParameters();
                parameters.Add("@RecordTime", DateTime.Now);
                parameters.Add("@FirstName", payload.FirstName);
                parameters.Add("@LastName", payload.LastName);
                parameters.Add("@Email", payload.Email);
                parameters.Add("@Phone", payload.Phone);
                returnValEmployee = await Task.FromResult(_dapper.Insert(employee, parameters));

                var employeeDetails = $"INSERT INTO {TableEnum.EmployeeDetails.ToString()} " +
                  $"(EmployeeID, PassportNumber) " +
                  $"VALUES (@EmployeeID, @PassportNumber);" +
                  $" SELECT CAST(SCOPE_IDENTITY() as int);";

                var parametersDetails = new DynamicParameters();
                parametersDetails.Add("@EmployeeID", returnValEmployee);
                parametersDetails.Add("@PassportNumber", payload.PassportNumber);
                returnVal = await Task.FromResult(_dapper.Insert(employeeDetails, parametersDetails));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
      
        public async Task<EmployeeModel> UpdateOnetoOneById(string id, EmployeeModel payload)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = $"UPDATE {TableEnum.Employee.ToString()} " +
                    $" SET FirstName = @FirstName, LastName = @LastName, Email = @Email, Phone = @Phone where EmployeeID = @EmployeeID; " +
                    $" UPDATE {TableEnum.EmployeeDetails.ToString()} SET PassportNumber = @PassportNumber  where EmployeeID = @EmployeeID";

                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", Convert.ToInt64(id), System.Data.DbType.Int64);
                parameters.Add("@FirstName", payload.FirstName);
                parameters.Add("@LastName", payload.LastName);
                parameters.Add("@Email", payload.Email);
                parameters.Add("@Phone", payload.Phone);
                parameters.Add("@PassportNumber", payload.PassportNumber);
                returnVal = await Task.FromResult(_dapper.Update<EmployeeModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        
        public async Task<EmployeeModel> DeleteOnetoOneById(string id)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sqlCheck = $"SELECT * FROM {TableEnum.Employee.ToString()} where EmployeeID = @EmployeeID";
                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", id);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sqlCheck, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
                else
                {
                    var sqlDelete = $"DELETE FROM {TableEnum.Employee.ToString()} where EmployeeID = @EmployeeID;";
                    await Task.FromResult(_dapper.Delete<EmployeeModel>(sqlDelete, parameters));
                    return returnVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        #endregion

        #region 'One To Many'
        public async Task<DTPagination<EmployeeModel>> GetOneToMany(SearchDefaultModel search, string orderBy = "Type", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC)
        {
            try
            {
                page_size = page_size <= 0 ? 10 : page_size;
                orderBy = string.IsNullOrEmpty(orderBy) ? "Type" : orderBy;

                var sql = @$"SELECT emp.* , emp_a.AddressID as AddressID , emp_a.Type as Type , emp_a.Address as Address
                                FROM {TableEnum.Employee} emp
                                RIGHT JOIN {TableEnum.EmployeeAddress} emp_a on emp.EmployeeID = emp_a.EmployeeID
                          where 1=1
                ";

                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(search.keyword))
                {
                    list_search_query.Add($" emp.FirstName LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp.LastName LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp_a.Type LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp_a.Address LIKE '%{search.keyword?.Trim()}%' ");
                }

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                long count = await Task.FromResult(_dapper.CountAll(sql));

                sql += $" ORDER BY {orderBy} {sort.ToString()} OFFSET {page_number} ROWS FETCH NEXT {page_size} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<EmployeeModel>(sql));

                return DTPagination<EmployeeModel>.Success(items, count, page_number, page_size);
            }
            catch (Exception ex)
            {
                return DTPagination<EmployeeModel>.Failure(new List<string>() { ex.Message });
            }
        }

        public async Task<EmployeeModel> getOneToManyByID(string AddressID)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = @$"SELECT emp.* FROM {TableEnum.EmployeeAddress} emp
                            where emp.AddressID = @AddressID";
                var parameters = new DynamicParameters();
                parameters.Add("@AddressID", AddressID);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
            }
            catch (Exception)
            {
                returnVal = new EmployeeModel();
            }

            return returnVal;
        }

        public async Task<EmployeeModel> getOneToManyCheckData(long EmployeeID, string Type)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = @$"SELECT emp.* FROM {TableEnum.EmployeeAddress} emp
                          where emp.EmployeeID = @EmployeeID AND emp.Type = @Type";
                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", EmployeeID);
                parameters.Add("@Type", Type);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
            }
            catch (Exception)
            {
                returnVal = new EmployeeModel();
            }

            return returnVal;
        }

        public async Task<long> CreateOneToMany(EmployeeModel payload)
        {
            long returnVal = 0;
            try
            {
                var employee = $"INSERT INTO {TableEnum.EmployeeAddress.ToString()} " +
                   $"(EmployeeID, Type, Address) " +
                   $"VALUES (@EmployeeID, @Type, @Address);" +
                   $" SELECT CAST(SCOPE_IDENTITY() as int);";

                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", payload.EmployeeID);
                parameters.Add("@Type", payload.Type);
                parameters.Add("@Address", payload.Address);
                returnVal = await Task.FromResult(_dapper.Insert(employee, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        public async Task<EmployeeModel> UpdateOneToManyById(string id, EmployeeModel payload)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = $"UPDATE {TableEnum.EmployeeAddress.ToString()} " +
                    $" SET Address = @Address where AddressID = @AddressID;";

                var parameters = new DynamicParameters();
                parameters.Add("@AddressID", Convert.ToInt64(id), System.Data.DbType.Int64);
                parameters.Add("@Address", payload.Address);
                returnVal = await Task.FromResult(_dapper.Update<EmployeeModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        public async Task<EmployeeModel> DeleteOneToManyById(string AddressID)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sqlCheck = $"SELECT * FROM {TableEnum.EmployeeAddress.ToString()} where AddressID = @AddressID";
                var parameters = new DynamicParameters();
                parameters.Add("@AddressID", AddressID);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sqlCheck, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
                else
                {
                    var sqlDelete = $"DELETE FROM {TableEnum.EmployeeAddress.ToString()} where AddressID = @AddressID;";
                    await Task.FromResult(_dapper.Delete<EmployeeModel>(sqlDelete, parameters));
                    return returnVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        #endregion

        #region 'Many To Many'
        public async Task<DTPagination<EmployeeModel>> GetManyToMany(SearchDefaultModel search, string orderBy = "Type", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC)
        {
            try
            {
                page_size = page_size <= 0 ? 10 : page_size;
                orderBy = string.IsNullOrEmpty(orderBy) ? "Type" : orderBy;

                var sql = @$"SELECT emp.* , emp_s.EmpSID as EmpSID, emp_s.SkillID as SkillID, emp_sk.Description as Description
                                FROM {TableEnum.Employee} emp
                                RIGHT JOIN {TableEnum.EmployeeSkill} emp_s on emp.EmployeeID = emp_s.EmployeeID
                                LEFT JOIN {TableEnum.SkillDetails} emp_sk on emp_s.SkillID = emp_sk.SkillID
                          where 1=1
                ";

                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(search.keyword))
                {
                    list_search_query.Add($" emp.FirstName LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp.LastName LIKE '%{search.keyword?.Trim()}%' ");
                    list_search_query.Add($" emp_sk.Description LIKE '%{search.keyword?.Trim()}%' ");
                }

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                long count = await Task.FromResult(_dapper.CountAll(sql));

                sql += $" ORDER BY {orderBy} {sort.ToString()} OFFSET {page_number} ROWS FETCH NEXT {page_size} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<EmployeeModel>(sql));

                return DTPagination<EmployeeModel>.Success(items, count, page_number, page_size);
            }
            catch (Exception ex)
            {
                return DTPagination<EmployeeModel>.Failure(new List<string>() { ex.Message });
            }
        }

        public async Task<EmployeeModel> getManyToManyByID(string EmpSID)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = @$"SELECT emp.* FROM {TableEnum.EmployeeSkill} emp
                            where emp.EmpSID = @EmpSID";
                var parameters = new DynamicParameters();
                parameters.Add("@EmpSID", EmpSID);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
            }
            catch (Exception)
            {
                returnVal = new EmployeeModel();
            }

            return returnVal;
        }

        public async Task<EmployeeModel> getManyToManyCheckData(long EmployeeID, long SkillID)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = @$"SELECT emp.* FROM {TableEnum.EmployeeSkill} emp
                          where emp.EmployeeID = @EmployeeID AND emp.SkillID = @SkillID";
                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", EmployeeID);
                parameters.Add("@SkillID", SkillID);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
            }
            catch (Exception)
            {
                returnVal = new EmployeeModel();
            }

            return returnVal;
        }

        public async Task<long> CreateManyToMany(EmployeeModel payload)
        {
            long returnVal = 0;
            try
            {
                var employee = $"INSERT INTO {TableEnum.EmployeeSkill.ToString()} " +
                   $"(EmployeeID, SkillID) " +
                   $"VALUES (@EmployeeID, @SkillID);" +
                   $" SELECT CAST(SCOPE_IDENTITY() as int);";

                var parameters = new DynamicParameters();
                parameters.Add("@EmployeeID", payload.EmployeeID);
                parameters.Add("@SkillID", payload.SkillID);
                returnVal = await Task.FromResult(_dapper.Insert(employee, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        public async Task<EmployeeModel> UpdateManyToManyById(string id, EmployeeModel payload)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sql = $"UPDATE {TableEnum.EmployeeSkill.ToString()} " +
                    $" SET SkillID = @SkillID where EmpSID = @EmpSID;";

                var parameters = new DynamicParameters();
                parameters.Add("@EmpSID", Convert.ToInt64(id), System.Data.DbType.Int64);
                parameters.Add("@SkillID", payload.SkillID);
                returnVal = await Task.FromResult(_dapper.Update<EmployeeModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        public async Task<EmployeeModel> DeleteManyToManyById(string id)
        {
            EmployeeModel returnVal = new EmployeeModel();
            try
            {
                var sqlCheck = $"SELECT * FROM {TableEnum.EmployeeSkill.ToString()} where EmpSID = @EmpSID";
                var parameters = new DynamicParameters();
                parameters.Add("@EmpSID", id);
                returnVal = await Task.FromResult(_dapper.Get<EmployeeModel>(sqlCheck, parameters));
                if (returnVal == null)
                {
                    returnVal = new EmployeeModel();
                }
                else
                {
                    var sqlDelete = $"DELETE FROM {TableEnum.EmployeeSkill.ToString()} where EmpSID = @EmpSID";
                    await Task.FromResult(_dapper.Delete<EmployeeModel>(sqlDelete, parameters));
                    return returnVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        #endregion
    }
}
