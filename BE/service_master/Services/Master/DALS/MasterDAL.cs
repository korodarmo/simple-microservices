﻿using Dapper;
using service_master.Helpers;
using service_master.Helpers.SqlServer.Dapper;
using service_master.Models;
using service_master.Services.Master.Interfaces;
using service_master.Services.Master.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace service_master.Services.Master.DALS
{
    public class MasterDAL : IMaster
    {
        private readonly IDapperMain _dapper;
        public MasterDAL(IDapperMain dapper) { _dapper = dapper; }

        #region 'Skill'
        public async Task<DTPagination<SkillModel>> GetSkillDatatable(SearchDefaultModel search, string orderBy = "Description", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC)
        {
            try
            {
                page_size = page_size <= 0 ? 10 : page_size;
                orderBy = string.IsNullOrEmpty(orderBy) ? "Description" : orderBy;

                var sql = @$"SELECT * FROM {TableEnum.SkillDetails} where 1=1";

                var list_search_query = new List<string>();

                if (!string.IsNullOrEmpty(search.keyword))
                {
                    list_search_query.Add($"Description LIKE '%{search.keyword?.Trim()}%' ");
                }

                if (list_search_query.Count() > 0)
                {
                    var search_query = " AND (";

                    for (int i = 0; i < list_search_query.Count; i++)
                    {
                        if (i > 0)
                        {
                            search_query += $" OR " + list_search_query[i];
                        }
                        else
                        {
                            search_query += list_search_query[i];
                        }
                    }

                    sql += search_query + ") ";
                }

                long count = await Task.FromResult(_dapper.CountAll(sql));

                sql += $" ORDER BY {orderBy} {sort.ToString()} OFFSET {page_number} ROWS FETCH NEXT {page_size} ROWS ONLY";
                var items = await Task.FromResult(_dapper.GetAll<SkillModel>(sql));

                return DTPagination<SkillModel>.Success(items, count, page_number, page_size);
            }
            catch (Exception ex)
            {
                return DTPagination<SkillModel>.Failure(new List<string>() { ex.Message });
            }
        }

        public async Task<List<SkillModel>> getListSkill(string filter)
        {
            List<SkillModel> returnVal = new List<SkillModel>();
            string filtering = string.IsNullOrEmpty(filter) ? " " : " and Description LIKE '%" + filter + "%' ";
            try
            {
                var sql = $@" SELECT * FROM {TableEnum.SkillDetails.ToString()}  where SkillID != 0 {filtering}";
                var parameters = new DynamicParameters();
                returnVal = await Task.FromResult(_dapper.GetAll<SkillModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return returnVal;
        }
       
        public async Task<SkillModel> getSkillByID(string id)
        {
            SkillModel returnVal = new SkillModel();
            try
            {
                var sql = @$"SELECT * FROM {TableEnum.SkillDetails} where SkillID = @SkillID";
                var parameters = new DynamicParameters();
                parameters.Add("@SkillID", id);
                returnVal = await Task.FromResult(_dapper.Get<SkillModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new SkillModel();
                }
            }
            catch (Exception)
            {
                returnVal = new SkillModel();
            }

            return returnVal;
        }
        public async Task<SkillModel> getSkillByName(string name)
        {
            SkillModel returnVal = new SkillModel();
            try
            {
                var sql = @$"SELECT TOP 1 * FROM {TableEnum.SkillDetails} where Description = @Description";
                var parameters = new DynamicParameters();
                parameters.Add("@Description", name);
                returnVal = await Task.FromResult(_dapper.Get<SkillModel>(sql, parameters));
                if (returnVal == null)
                {
                    returnVal = new SkillModel();
                }
            }
            catch (Exception)
            {
                returnVal = new SkillModel();
            }

            return returnVal;
        }

        public async Task<long> CreateSkill(SkillModel payload)
        {
            long returnVal = 0;
            try
            {
                var sql = $"INSERT INTO {TableEnum.SkillDetails.ToString()} " +
                   $"(Description) " +
                   $"VALUES (@Description);" +
                   $" SELECT CAST(SCOPE_IDENTITY() as int);";

                var parameters = new DynamicParameters();
                parameters.Add("@Description", payload.Description);
                returnVal = await Task.FromResult(_dapper.Insert(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
      
        public async Task<SkillModel> UpdateSkillById(string id, SkillModel payload)
        {
            SkillModel returnVal = new SkillModel();
            try
            {
                var sql = $"UPDATE {TableEnum.SkillDetails.ToString()} " +
                    $" SET Description = @Description where SkillID = @SkillID; ";

                var parameters = new DynamicParameters();
                parameters.Add("@SkillID", id);
                parameters.Add("@Description", payload.Description);
                returnVal = await Task.FromResult(_dapper.Update<SkillModel>(sql, parameters));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }
        
        public async Task<SkillModel> DeleteSkillById(string id)
        {
            SkillModel returnVal = new SkillModel();
            try
            {
                var sqlCheck = $"SELECT * FROM {TableEnum.SkillDetails.ToString()} where SkillID = @SkillID";
                var parameters = new DynamicParameters();
                parameters.Add("@SkillID", id);
                returnVal = await Task.FromResult(_dapper.Get<SkillModel>(sqlCheck, parameters));
                if (returnVal == null)
                {
                    returnVal = new SkillModel();
                }
                else
                {
                    var sqlDelete = $"DELETE FROM {TableEnum.SkillDetails.ToString()} where SkillID = @SkillID;";
                    await Task.FromResult(_dapper.Delete<SkillModel>(sqlDelete, parameters));
                    return returnVal;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnVal;
        }

        #endregion

    }
}
