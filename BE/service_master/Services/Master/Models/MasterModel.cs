﻿using System;
using System.Text.Json.Serialization;

namespace service_master.Services.Master.Models
{
    public class SkillModel
    {
        [JsonIgnore]
        public long SkillID { get; set; }
        public string Description { get; set; }
    }

}
