﻿using service_master.Helpers;
using service_master.Models;
using service_master.Services.Master.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace service_master.Services.Master.Interfaces
{
    public interface IMaster
    {
        Task<DTPagination<SkillModel>> GetSkillDatatable(SearchDefaultModel search, string orderBy = "FirstName", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC);
        Task<List<SkillModel>> getListSkill(string filter);
        Task<SkillModel> getSkillByID(string id);
        Task<SkillModel> getSkillByName(string name);
        Task<long> CreateSkill(SkillModel payload);
        Task<SkillModel> UpdateSkillById(string id, SkillModel payload);
        Task<SkillModel> DeleteSkillById(string id);


    }
}