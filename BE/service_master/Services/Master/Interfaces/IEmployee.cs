﻿using service_master.Helpers;
using service_master.Models;
using service_master.Services.Master.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace service_master.Services.Master.Interfaces
{
    public interface IEmployee
    {
        Task<DTPagination<EmployeeModel>> GetOnetoOne(SearchDefaultModel search, string orderBy = "FirstName", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC);
        Task<List<EmployeeModel>> getListEmployee(string filter);
        Task<EmployeeModel> getOnetoOneByID(string id);
        Task<EmployeeModel> getCheckData(string FirstName, string Lastname, string PassportNumber);
        Task<long> CreateOnetoOne(EmployeeModel payload);
        Task<EmployeeModel> UpdateOnetoOneById(string id, EmployeeModel payload);
        Task<EmployeeModel> DeleteOnetoOneById(string id);

        Task<DTPagination<EmployeeModel>> GetOneToMany(SearchDefaultModel search, string orderBy = "Type", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC);
        Task<EmployeeModel> getOneToManyByID(string id);
        Task<EmployeeModel> getOneToManyCheckData(long EmployeeID, string Type);
        Task<long> CreateOneToMany(EmployeeModel payload);
        Task<EmployeeModel> UpdateOneToManyById(string id, EmployeeModel payload);
        Task<EmployeeModel> DeleteOneToManyById(string id);

        Task<DTPagination<EmployeeModel>> GetManyToMany(SearchDefaultModel search, string orderBy = "Type", int page_number = 1, int page_size = 10, SortEnum sort = SortEnum.DESC);
        Task<EmployeeModel> getManyToManyByID(string id);
        Task<EmployeeModel> getManyToManyCheckData(long EmployeeID, long SkillID);
        Task<long> CreateManyToMany(EmployeeModel payload);
        Task<EmployeeModel> UpdateManyToManyById(string id, EmployeeModel payload);
        Task<EmployeeModel> DeleteManyToManyById(string id);
    }
}