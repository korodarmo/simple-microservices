using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using service_master.Middlewares;
using System;
using System.IO;
using service_master.Services.Master.Interfaces;
using service_master.Services.Master.DALS;
using Coravel;
using service_master.Helpers.SqlServer.Dapper;
using service_master.Helpers;

namespace service_master
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            Console.WriteLine("APP UPDATED :  10122022 15:11");
            services.AddControllers();
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Data Query Service API", Version = "v1" });
                c.EnableAnnotations();
                c.AddSecurityDefinition("Bearer",
                new OpenApiSecurityScheme
                {
                    Description = "Silahkan isi dengan API KEY service yang diterima...",
                    Name = "ApiKey",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                });

            });

            services.AddRouting(r => r.SuppressCheckForUnhandledSecurityMetadata = true);
          
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddMvcCore(options =>
            {
                options.ModelBinderProviders.Insert(0, new HeaderComplexModelBinderProvider());
            });

            services.AddScheduler();
            services.AddScoped<IDapperMain, DapperrMain>();
            services.AddTransient<IEmployee, EmployeeDAL>();
            services.AddTransient<IMaster, MasterDAL>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Data Query Service API v1"));

            app.UseMiddleware<SecurityMiddleware>();

            app.UseRouting();

            app.UseCors("servicepolicy");

            app.Use((context, next) =>
            {
                context.Items["__corsmiddlewareinvoked"] = true;
                return next();
            });

            app.UseAuthorization();
            app.UseStaticFiles();
            var root = Directory.GetCurrentDirectory();
            var dotenv = Path.Combine(root, ".env");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            var provider = app.ApplicationServices;
            provider.UseScheduler(scheduler =>
            {
            });
        }
    }
}
