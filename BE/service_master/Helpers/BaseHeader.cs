﻿using Microsoft.AspNetCore.Mvc;

namespace service_master.Helpers
{
    public class BaseHeader
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string RoleAccess { get; set; }
        public string RoleID { get; set; }
        public string Rolename { get; set; }
        public string Instance { get; set; }
        public int OrganizationID { get; set; }
        public string OrganizationName { get; set; }
        public string IpAddress { get; set; }
        public string Line { get; set; }
        public string Competency { get; set; }
    }
}
