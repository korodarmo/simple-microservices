﻿namespace service_master.Helpers
{
    public enum TableEnum
    {
        Employee,
        EmployeeAddress,
        EmployeeDetails,
        EmployeeSkill,
        SkillDetails
    }
}