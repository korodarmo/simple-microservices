﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using System.IO;
using System;
using Microsoft.Data.SqlClient;

namespace service_master.Helpers.SqlServer.Database
{
    public class MainContext
    {
        public string ConnectionString { get; set; }

        public MainContext()
        {
            string connectionString = GetConnstring();
            this.ConnectionString = connectionString;
        }

        public SqlConnection GetConnection() { return new SqlConnection(ConnectionString); }

        private static string GetConnstring()
        {
            string Server = System.Environment.GetEnvironmentVariable("MST_DB_SERVER");
            string Db = System.Environment.GetEnvironmentVariable("MST_DB_NAME");
            string User = System.Environment.GetEnvironmentVariable("MST_DB_USER");
            string Password = System.Environment.GetEnvironmentVariable("MST_DB_PASS");
            string Connstring = $@"Data Source={Server};Initial Catalog={Db};Persist Security Info=True;User ID={User};Password={Password};TrustServerCertificate=True";
            Console.WriteLine("Connstring" + Connstring);
            return Connstring;
        }
    }
}
