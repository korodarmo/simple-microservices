﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;

namespace service_master.Helpers.SqlServer.Dapper
{
    public interface IDapperMain : IDisposable
    {
        bool ExistData<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        int CountAll(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        int CountAll<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T Get<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        List<T> GetAll<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        int Execute(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        long Insert(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T Update<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
        T Delete<T>(string sqlCommand, DynamicParameters parms = null, CommandType commandType = CommandType.Text);
    }
}