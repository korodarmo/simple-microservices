﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace service_master.Helpers
{
    public class DTPagination<T>
    {
        public DTPagination(List<T> data)
        {
            Data = data;
        }
        public HttpStatusCode StatusCode { get; set; }
        public List<T> Data { get; set; }
        public bool failed => !Succeeded;
        public string Message { get; set; }

        public bool Succeeded { get; set; }

        internal DTPagination(bool succeeded, List<T> data = default, List<string> messages = null, long count = 0, int page = 1, int pageSize = 10)
        {
            Data = data;
            Page = page;
            Succeeded = succeeded;
            Message = messages?.FirstOrDefault();
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            TotalCount = count;
        }
        public static DTPagination<T> Failure(List<string> messages)
        {
            return new DTPagination<T>(false, default, messages);
        }

        public static DTPagination<T> Success(List<T> data, long count, int page, int pageSize)
        {

            return new DTPagination<T>(true, data, null, count, page, pageSize);
        }
        public int Page { get; set; }

        public int TotalPages { get; set; }

        public long TotalCount { get; set; }

        public bool HasPreviousPage => Page > 1;

        public bool HasNextPage => Page < TotalPages;
    }
}
