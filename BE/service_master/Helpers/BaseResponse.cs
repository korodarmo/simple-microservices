﻿namespace service_master.Helpers
{
    public class Constants
    {
        public static string errorcode = "422";
        public static string successcode = "0";

    }
    public class BaseResponse
    {
        public bool success { get; set; }
        public string code { get; set; }
        public int row { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }

    public class DataTableResponse
    {
        public bool success { get; set; }
        public string code { get; set; }
        public int row { get; set; }
        public string message { get; set; }
        public int draw { get; set; }
        public object data { get; set; }
        public long recordsFiltered { get; set; }
        public long recordsTotal { get; set; }
    }
    public class AuditTrailsResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
