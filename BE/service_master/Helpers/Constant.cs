﻿namespace service_master.Helpers
{    public class Constant
    {
        public static string messageSuccess = "Success";
        public static string messageFailed = "Success";
        public static string messageExist = "Data already exists!";
        public static string messageNotExist = "Data not exists!";
        public static string codeSuccess = "1";
        public static string codeFailed = "-1";
        public static string codeDataNotValid = "422 ";
        public static string messageDataNotValid = "Data not valid!";
        public static string headerInvalid = "Your Header is Invalid";
        public static string mappinginvalid = "Mapping data tidak ditemukan, silahkan hubungi administrator";


        public static string createData = "Create";
        public static string updateData = "Update";
        public static string deleteData = "Delete";

        public static string invalidTypeRequest = "Invalid request type!";


        #region 'Exchange & Queue Master RabbitMQ'
        public static string userCredentialMaster = "userCredential";
        public static string dataMachineMaster = "Ebridging-RA-Parent-Asset-Num";
        public static string dataItemMaster = "Ebridging-M-Item";
        public static string dataOperationMaster = "Ebridging-M-Operation-Class";
        public static string dataAreaMaster = "Ebridging-RA-AREA";
        public static string dataInventoryPartMaster = "Ebridging-Inv-Item-Sparepart";
        #endregion

        #region 'Exchange & Queue Service'
        public static string userCredential = "userCredential_EPM_ServiceMaster";
        public static string dataMachine = "Ebridging-RA-Parent-Asset-Num_EPM_ServiceMaster";
        public static string dataItem = "Ebridging-M-Item_EPM_ServiceMaster";
        public static string dataOperation = "Ebridging-M-Operation-Class_EPM_ServiceMaster";
        public static string dataArea = "Ebridging-RA-AREA_EPM_ServiceMaster";
        public static string dataInventoryPart = "Ebridging-Inv-Item-Sparepart_EPM_ServiceMaster";
        #endregion

    }
}
