﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using service_master.Helpers;
using service_master.Services.Master.Interfaces;
using service_master.Services.Master.Models;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using service_master.Models;
using service_master.Middlewares;

namespace service_master.Controllers
{
    [Route("/employee")]
    [EnableCors("ServicePolicy")]
    [DisplayName("Employee")]
    public class EmployeeController : Controller
    {
        private readonly IEmployee iEmployee;

        public EmployeeController(IEmployee _iEmployee)
        {
            iEmployee = _iEmployee;
        }

        #region 'One To One'
        [HttpPost("/employee/onetoone/getDatatables")]
        [Authorize]
        public async Task<IActionResult> getOneToOneDatatables(
            [FromBody] SearchDefaultModel search,
            string orderBy,
            SortEnum sort,
            int pageNumber,
            int pageSize)
        {
            try
            {
                var data = await iEmployee.GetOnetoOne(search, orderBy, pageNumber, pageSize, sort);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("/employee/onetoone/list")]
        [Authorize]
        public async Task<IActionResult> getListEmployee(string term)
        {
            string Message = "Failed";
            List<EmployeeModel> vResult = new List<EmployeeModel>();
            try
            {
                Message = "Success";
                vResult = await iEmployee.getListEmployee(term);
            }
            catch (Exception ex)
            {
                Message = ex.Message.ToString();
            }

            return Json(new
            {
                Message = Message,
                Data = vResult
            });
        }
        [HttpGet("/employee/onetoone/{id}")]
        [Authorize]
        public async Task<IActionResult> getOnetoOneByID(string id)
        {
            try
            {
                var _data = await iEmployee.getOnetoOneByID(id);
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }
        
        [HttpPost("/employee/onetoone")]
        [Authorize]
        public async Task<IActionResult> createOnetoOne([FromBody] EmployeeModel Data)
        {
            try
            {
                EmployeeModel _data = new EmployeeModel();
                var checkData = await iEmployee.getCheckData(Data.FirstName, Data.LastName, Data.PassportNumber);
                if (checkData.EmployeeID > 0)
                {
                    if (checkData.FirstName.Equals(Data.FirstName) && checkData.LastName.Equals(Data.LastName))
                    {
                        throw new Exception("First and Last name already exist... ");
                    }
                    else if (checkData.PassportNumber.Equals(Data.PassportNumber))
                    {
                        throw new Exception("Passport Number already exist... ");
                    }
                    else
                    {
                        throw new Exception("Data Already Exist ");
                    }
                }
                else
                {
                    var result = await iEmployee.CreateOnetoOne(Data);
                    if (result > 0)
                    {
                        _data = await iEmployee.getOnetoOneByID(result.ToString());
                    }
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPatch("/employee/onetoone/{id}")]
        [Authorize]
        public async Task<IActionResult> updateOnetoOneById(string id, [FromBody] EmployeeModel Data)
        {
            try
            {
                var _data = await iEmployee.getOnetoOneByID(id);
                var dataBefore = _data;
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                else
                {
                    _data = await iEmployee.UpdateOnetoOneById(id, Data);
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpDelete("/employee/onetoone/{id}")]
        [Authorize]
        public async Task<IActionResult> deleteOnetoOneById(string id)
        {
            try
            {
                var _data = await iEmployee.DeleteOnetoOneById(id);
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }
        #endregion

        #region 'One To Many'
        [HttpPost("/employee/onetomany/getDatatables")]
        [Authorize]
        public async Task<IActionResult> getOneToManyDatatables(
            [FromBody] SearchDefaultModel search,
            string orderBy,
            SortEnum sort,
            int pageNumber,
            int pageSize)
        {
            try
            {
                var data = await iEmployee.GetOneToMany(search, orderBy, pageNumber, pageSize, sort);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("/employee/onetomany/{id}")]
        [Authorize]
        public async Task<IActionResult> getOneToManyByID(string id)
        {
            try
            {
                var _data = await iEmployee.getOneToManyByID(id);
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPost("/employee/onetomany")]
        [Authorize]
        public async Task<IActionResult> createOneToMany([FromBody] EmployeeModel Data)
        {
            try
            {
                EmployeeModel _data = new EmployeeModel();
                var checkData = await iEmployee.getOneToManyCheckData(Data.EmployeeID, Data.Type);
                if (checkData.EmployeeID > 0)
                {
                    throw new Exception("Data Already Exist ");
                }
                else
                {
                    var result = await iEmployee.CreateOneToMany(Data);
                    if (result > 0)
                    {
                        _data = await iEmployee.getOneToManyByID(result.ToString());
                    }
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPatch("/employee/onetomany/{id}")]
        [Authorize]
        public async Task<IActionResult> updateOneToManyById(string id, [FromBody] EmployeeModel Data)
        {
            try
            {
                var _data = await iEmployee.getOneToManyByID(id);
                var dataBefore = _data;
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                else
                {
                    _data = await iEmployee.UpdateOneToManyById(id, Data);
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpDelete("/employee/onetomany/{id}")]
        [Authorize]
        public async Task<IActionResult> deleteOneToManyById(string id)
        {
            try
            {
                var _data = await iEmployee.DeleteOneToManyById(id);
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }
        #endregion

        #region 'Many To Many'
        [HttpPost("/employee/manytomany/getDatatables")]
        [Authorize]
        public async Task<IActionResult> getManyToManyDatatables(
            [FromBody] SearchDefaultModel search,
            string orderBy,
            SortEnum sort,
            int pageNumber,
            int pageSize)
        {
            try
            {
                var data = await iEmployee.GetManyToMany(search, orderBy, pageNumber, pageSize, sort);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("/employee/ManyToMany/{id}")]
        [Authorize]
        public async Task<IActionResult> getManyToManyByID(string id)
        {
            try
            {
                var _data = await iEmployee.getManyToManyByID(id);
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPost("/employee/ManyToMany")]
        [Authorize]
        public async Task<IActionResult> createManyToMany([FromBody] EmployeeModel Data)
        {
            try
            {
                EmployeeModel _data = new EmployeeModel();
                var checkData = await iEmployee.getManyToManyCheckData(Data.EmployeeID, Data.SkillID);
                if (checkData.EmployeeID > 0)
                {
                    throw new Exception("Data Already Exist ");
                }
                else
                {
                    var result = await iEmployee.CreateManyToMany(Data);
                    if (result > 0)
                    {
                        _data = await iEmployee.getManyToManyByID(result.ToString());
                    }
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPatch("/employee/ManyToMany/{id}")]
        [Authorize]
        public async Task<IActionResult> updateManyToManyById(string id, [FromBody] EmployeeModel Data)
        {
            try
            {
                var _data = await iEmployee.getManyToManyByID(id);
                var dataBefore = _data;
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                else
                {
                    _data = await iEmployee.UpdateManyToManyById(id, Data);
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpDelete("/employee/ManyToMany/{id}")]
        [Authorize]
        public async Task<IActionResult> deleteManyToManyById(string id)
        {
            try
            {
                var _data = await iEmployee.DeleteManyToManyById(id);
                if (_data.EmployeeID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }
        #endregion
    }
}
