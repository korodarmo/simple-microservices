﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using service_master.Helpers;
using service_master.Services.Master.Interfaces;
using service_master.Services.Master.Models;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using service_master.Models;
using service_master.Middlewares;

namespace service_master.Controllers
{
    [Route("/master")]
    [EnableCors("ServicePolicy")]
    [DisplayName("Master")]
    public class MasterController : Controller
    {
        private readonly IMaster iMaster;

        public MasterController(IMaster _iMaster)
        {
            iMaster = _iMaster;
        }

        #region 'Skill'
        [HttpPost("/master/skill/getDatatables")]
        [Authorize]
        public async Task<IActionResult> getDatatables(
            [FromBody] SearchDefaultModel search,
            string orderBy,
            SortEnum sort,
            int pageNumber,
            int pageSize)
        {
            try
            {
                var data = await iMaster.GetSkillDatatable(search, orderBy, pageNumber, pageSize, sort);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpGet("/master/skill/listSkill")]
        [Authorize]
        public async Task<IActionResult> getListSkill(string term)
        {
            string Message = "Failed";
            List<SkillModel> vResult = new List<SkillModel>();
            try
            {
                Message = "Success";
                vResult = await iMaster.getListSkill(term);
            }
            catch (Exception ex)
            {
                Message = ex.Message.ToString();
            }

            return Json(new
            {
                Message = Message,
                Data = vResult
            });
        }

        [HttpGet("/master/skill/{id}")]
        [Authorize]
        public async Task<IActionResult> getSkillByID(string id)
        {
            try
            {
                var _data = await iMaster.getSkillByID(id);
                if (_data.SkillID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }
        
        [HttpPost("/master/skill")]
        [Authorize]
        public async Task<IActionResult> createCompetency([FromBody] SkillModel Data)
        {
            try
            {
                SkillModel _data = new SkillModel();
                var checkData = await iMaster.getSkillByName(Data.Description);
                if (checkData.SkillID > 0)
                {
                    throw new Exception("Skill already exist...");
                }
                else
                {
                    var result = await iMaster.CreateSkill(Data);
                    if (result > 0)
                    {
                        _data = await iMaster.getSkillByID(result.ToString());
                    }
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpPatch("/master/skill/{id}")]
        [Authorize]
        public async Task<IActionResult> updateOnetoOneById(string id, [FromBody] SkillModel Data)
        {
            try
            {
                var _data = await iMaster.getSkillByID(id);
                var dataBefore = _data;
                if (_data.SkillID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                else
                {
                    _data = await iMaster.UpdateSkillById(id, Data);
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }

        [HttpDelete("/master/skill/{id}")]
        [Authorize]
        public async Task<IActionResult> deleteSkillById(string id)
        {
            try
            {
                var _data = await iMaster.DeleteSkillById(id);
                if (_data.SkillID == 0)
                {
                    throw new Exception("Record Not Found");
                }
                return Ok(new BaseResponse()
                {
                    success = true,
                    code = Constants.successcode,
                    message = "Berhasil",
                    data = _data
                });
            }
            catch (Exception ex)
            {
                return Ok(new BaseResponse()
                {
                    success = false,
                    code = Constants.errorcode,
                    message = ex.Message,
                    data = null
                });
            }
        }
        #endregion
    }
}
