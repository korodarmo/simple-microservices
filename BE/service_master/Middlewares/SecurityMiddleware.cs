﻿using LiteDB;
using Microsoft.AspNetCore.Http;
using System.Linq;
using System.Threading.Tasks;

namespace service_master.Middlewares
{
    public class SecurityMiddleware
    {
        private readonly RequestDelegate _next;

        public SecurityMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["ApiKey"].FirstOrDefault()?.Split(" ").Last();
            await _next(context);
        }

    }
}
