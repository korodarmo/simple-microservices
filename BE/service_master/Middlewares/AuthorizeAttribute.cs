﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using service_master.Models;
using System;
using service_master.Helpers;
using System.Linq;

namespace service_master.Middlewares
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var AppKey = context.HttpContext.Request.Headers["ApiKey"].FirstOrDefault()?.Split(" ").Last();
            string appKeys = Environment.GetEnvironmentVariable("MST_APP_KEY"); 
            if (appKeys != AppKey || string.IsNullOrEmpty(AppKey))
            {
                context.Result = new JsonResult(new BaseResponse()
                {
                    success = false,
                    code = "99",
                    message = "Maaf, anda tidak memiliki akses.",
                    row = 0,
                    data = null
                })
                { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }
    }
}
