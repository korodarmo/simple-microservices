using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace service_master
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    var FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "")).Root;
                    // var baseurl = new ConfigurationBuilder().AddJsonFile(FileProvider + "appsettings.json").Build().GetSection("application");
#if DEBUG                    
                    // webBuilder.UseStartup<Startup>().UseUrls(baseurl["baseurl"]);
                    webBuilder.UseStartup<Startup>().UseUrls("http://localhost:20000");
#else
                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory())
                        .UseIISIntegration()
                        .UseStartup<Startup>();
#endif
                });
    }
}
