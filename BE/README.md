Backend
====================

**Dev on testing**
http://localhost:20000

How To Build
========================
- Import Database
- Please change value on profiles with your local configuration
```shell
	Configuration location : ./service_master/Properties/launchSettings

	"MST_DB_SERVER": "PLEASE_CHANGES_ME",
    "MST_DB_NAME": "PLEASE_CHANGE_ME",
    "MST_DB_USER": "PLEASE_CHANGE_ME",
    "MST_DB_PASS": "PLEASE_CHANGE_ME",
```